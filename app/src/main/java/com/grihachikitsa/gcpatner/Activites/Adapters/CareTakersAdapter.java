package com.grihachikitsa.gcpatner.Activites.Adapters;

/**
 * Created by Nikil on 12/14/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.gcpatner.Activites.Activites.EditCareTaker;
import com.grihachikitsa.gcpatner.Activites.Activites.EditCareTakerDetailsActivity;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Config;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CareTakersAdapter extends RecyclerView.Adapter<CareTakersAdapter.MyViewHolder> {

    private List<CareTakersResponseModel> careTakersResponseModels;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, gender, email;
        public Button edit, delete;
        public CircleImageView profilePic;

        public MyViewHolder(View view) {
            super(view);
            //careType = (TextView) view.findViewById(R.id.careType);
            name = (TextView) view.findViewById(R.id.name);
            gender = (TextView) view.findViewById(R.id.gender);
            email = (TextView) view.findViewById(R.id.email);
            edit = (Button) view.findViewById(R.id.edit);
            delete = (Button) view.findViewById(R.id.delete);
            profilePic = (CircleImageView) view.findViewById(R.id.profilepic);

        }
    }


    public CareTakersAdapter(List<CareTakersResponseModel> moviesList, Context context) {
        this.careTakersResponseModels = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_caretakers, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final CareTakersResponseModel careTakersResponseModel = careTakersResponseModels.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());

        holder.name.setText(checkNull(careTakersResponseModel.getCaretaker_name()));
        holder.email.setText("Email: " + checkNullNA(careTakersResponseModel.getEmail()));
        holder.gender.setText("Gender: " + checkNullNA(careTakersResponseModel.getGender()));
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, EditCareTakerDetailsActivity.class).putExtra("careTaker", careTakersResponseModel.getId()));
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deleteCareTaker(careTakersResponseModel.getId(),careTakersResponseModel.getCaretaker_name(),position);
            }
        });

        Picasso.with(context).load(Config.BASE_URL + careTakersResponseModel.getProfile_pic()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person).into(holder.profilePic);


    }

    public void deleteCareTaker(final String id, String name, final int position) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_logout);
        Button logout = (Button) dialog.findViewById(R.id.logout);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        logout.setText("Delete");
        title.setText("Are you sure, you want to remove "+name);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);


                Call<RegisterResponseModel> call = apiService.deleteCareTaker(id);
                call.enqueue(new Callback<RegisterResponseModel>() {
                    @Override
                    public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                        customProgressDialog.cancel();
                        if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                           careTakersResponseModels.remove(position);;
                            notifyDataSetChanged();

                            if (response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Details updated", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (response.body()!=null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                                Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                        customProgressDialog.cancel();
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();


            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((EditCareTaker) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public String checkNullNA(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "NA";
    }

    @Override
    public int getItemCount() {
        return careTakersResponseModels.size();
    }

    public void updateRefernceStatus() {

    }

}
