package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

import com.grihachikitsa.gcpatner.Activites.Models.NewLeadsModel;

import java.util.List;

/**
 * Created by Nikil on 12/25/2016.
 */
public class NewLeadsResponseModel {



    private String error;
    private String age;
    private String gender;
    private String services;
    private String duration;
    private String latlong;
    private String first_name;

    public String getFirst_language() {
        return first_language;
    }

    public void setFirst_language(String first_language) {
        this.first_language = first_language;
    }

    public String getSecond_language() {
        return second_language;
    }

    public void setSecond_language(String second_language) {
        this.second_language = second_language;
    }

    public String getThird_language() {
        return third_language;
    }

    public void setThird_language(String third_language) {
        this.third_language = third_language;
    }

    private String last_name,first_language,second_language,third_language;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address;

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    private String project_id;

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    private String weight;
    private List<NewLeadsResponseModel> tasks;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public List<NewLeadsResponseModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<NewLeadsResponseModel> tasks) {
        this.tasks = tasks;
    }
}
