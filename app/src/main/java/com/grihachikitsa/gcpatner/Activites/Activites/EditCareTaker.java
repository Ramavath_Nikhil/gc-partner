package com.grihachikitsa.gcpatner.Activites.Activites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.grihachikitsa.gcpatner.Activites.Adapters.CareTakersAdapter;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class EditCareTaker extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private CareTakersAdapter mAdapter;
    private List<CareTakersResponseModel> careTakersResponseModels = new ArrayList<>();
    private ErrorView errorView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_care_taker);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }


        elementsInilization();
        getOngoingProjectsData();
    }



    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(EditCareTaker.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CareTakersAdapter(careTakersResponseModels, EditCareTaker.this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);

        //ERRORVIEW INTITLIZATION
        errorView = (ErrorView) findViewById(R.id.error_view);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                errorView.setVisibility(View.GONE);
                getOngoingProjectsData();
            }
        });

        //SWIPE REFRESH LAYOUT INTILIZATION
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefreshkayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getOngoingProjectsData();
            }
        });

    }

    public void getOngoingProjectsData() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<CareTakersResponseModel> call = apiService.getCareTakersOfAnOrganisation(Prefs.getString("organisationID", ""));
        call.enqueue(new Callback<CareTakersResponseModel>() {
            @Override
            public void onResponse(Call<CareTakersResponseModel> call, Response<CareTakersResponseModel> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {




                        if (careTakersResponseModels != null && careTakersResponseModels.size() > 0) {
                            careTakersResponseModels.clear();
                            mAdapter.notifyDataSetChanged();
                        }


                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        errorView.setVisibility(View.GONE);
                        careTakersResponseModels.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setTitle("Caretakers not available");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setTitle("Something went wrong");

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<CareTakersResponseModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                errorView.setTitle("Something went wrong");

            }


        });
    }

}
