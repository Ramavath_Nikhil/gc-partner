package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nikil on 12/25/2016.
 */
public class CareTakersResponseModel implements Serializable {



    private String id;
    private String organisation_id;
    private String caretaker_name;
    private String mobile;
    private String gender;
    private String status;
    private String error;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    private String profile_pic;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;
    private List<CareTakersResponseModel> tasks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrganisation_id() {
        return organisation_id;
    }

    public void setOrganisation_id(String organisation_id) {
        this.organisation_id = organisation_id;
    }

    public String getCaretaker_name() {
        return caretaker_name;
    }

    public void setCaretaker_name(String caretaker_name) {
        this.caretaker_name = caretaker_name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<CareTakersResponseModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<CareTakersResponseModel> tasks) {
        this.tasks = tasks;
    }
}
