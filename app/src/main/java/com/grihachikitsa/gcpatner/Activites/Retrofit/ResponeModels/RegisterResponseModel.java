package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

/**
 * Created by Nikil on 12/27/2016.
 */
public class RegisterResponseModel {



    private String error;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
