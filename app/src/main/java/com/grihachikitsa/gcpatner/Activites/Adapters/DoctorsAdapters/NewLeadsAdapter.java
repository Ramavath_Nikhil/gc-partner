package com.grihachikitsa.gcpatner.Activites.Adapters.DoctorsAdapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.gcpatner.Activites.Fragments.NewLeadsFragment;

import com.grihachikitsa.gcpatner.Activites.Models.DocotorsModel.NewLeadsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Nikil on 2/18/2017.
 */
public class NewLeadsAdapter extends RecyclerView.Adapter<NewLeadsAdapter.MyViewHolder> {

    public List<NewLeadsModel> newLeadsModels;
    public Context context;
    public boolean is_newLeads;

    public NewLeadsAdapter(List<NewLeadsModel> newLeadsResponseModels, Context context, boolean is_newLeads) {
        this.newLeadsModels = newLeadsResponseModels;
        this.context = context;
        this.is_newLeads = is_newLeads;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_doctors_newleads, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final NewLeadsModel newLeadsModel = newLeadsModels.get(position);

        if (newLeadsModel.getUser_name() != null && !newLeadsModel.getUser_name().isEmpty()) {
            holder.patientName.setText(newLeadsModel.getUser_name());
        }
        if (newLeadsModel.getAddress() != null && !newLeadsModel.getAddress().isEmpty()) {
            holder.patientAddress.setText(newLeadsModel.getAddress());
        }

        if (newLeadsModel.getTimeslot_sub() != null && !newLeadsModel.getTimeslot_sub().isEmpty()) {
            holder.timeSlot.setText("Slot: "+newLeadsModel.getTimeslot_sub());
        }

        if (newLeadsModel.getDate() != null && !newLeadsModel.getDate().isEmpty()) {
            holder.appointmentDate.setText("Date: "+newLeadsModel.getDate());
        }


        if (is_newLeads) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    cancelAppointment(newLeadsModel);
                }
            });
        } else {
            holder.cancel.setVisibility(View.GONE);
        }
    }


    public void cancelAppointment(final NewLeadsModel newLeadsModel) {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.cancelDoctorAppointment(newLeadsModel.getDoctor_id(), newLeadsModel.getId());
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                customProgressDialog.cancel();

                if (response.body().getError().equalsIgnoreCase("false")) {
                    Toast.makeText(context, "Appointment has been cancelled", Toast.LENGTH_SHORT).show();
                    newLeadsModels.remove(newLeadsModel);
                    notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                customProgressDialog.cancel();

                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return newLeadsModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView patientName, patientAddress, appointmentDate, timeSlot;
        public Button cancel;


        public MyViewHolder(View view) {
            super(view);

            patientName = (TextView) view.findViewById(R.id.patientName);
            patientAddress = (TextView) view.findViewById(R.id.patientAddress);
            cancel = (Button) view.findViewById(R.id.cancelAppointment);
            appointmentDate = (TextView) view.findViewById(R.id.appointmentDate);
            timeSlot = (TextView) view.findViewById(R.id.timeSlot);
        }
    }
}
