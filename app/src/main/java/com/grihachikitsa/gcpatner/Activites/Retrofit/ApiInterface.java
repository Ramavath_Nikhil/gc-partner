package com.grihachikitsa.gcpatner.Activites.Retrofit;

import com.grihachikitsa.gcpatner.Activites.Models.DocotorsModel.NewLeadsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.AvailableCareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakerCompleteDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.DoctorLoginResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.DoctorsSpecilizationModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.LoginResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.NewLeadsResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.OngoingProjectsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProfileResponseActivity;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProjectCompletDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.QuotesModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.UpdateCareTakerDetailsModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Nikil on 12/25/2016.
 */
public interface ApiInterface {

    @FormUrlEncoded
    @POST("organisationLogin")
    Call<LoginResponseModel> organisationLogin(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("doctorlogin")
    Call<DoctorLoginResponseModel> doctorlogin(@Field("email") String email, @Field("password") String password);

    @GET("getPendingProjectForOrganisation")
    Call<NewLeadsResponseModel> getPendingProjectForOrganisation(@Query("organistion_id") String organistion_id);

    @GET("fetchDotorAppointments")
    Call<NewLeadsModel> fetchDotorAppointments(@Query("doctor_id") String doctor_id);

    @GET("fetchDotorPastAppointments")
    Call<NewLeadsModel> fetchDotorPastAppointments(@Query("doctor_id") String doctor_id);


    @GET("getProjectsUnderOrganisation")
    Call<OngoingProjectsModel> getProjectsUnderOrganisation(@Query("organistion_id") String organistion_id);

    @GET("getProjectsOfOrganisation")
    Call<OngoingProjectsModel> getProjectsOfOrganisation(@Query("organistion_id") String organistion_id);

    @GET("getCareTakersOfAnOrganisation")
    Call<CareTakersResponseModel> getCareTakersOfAnOrganisation(@Query("organistion_id") String organistion_id);

    @GET("getQuotedProjectsByOrganisation")
    Call<QuotesModel> getQuotedProjectsByOrganisation(@Query("organisation_id") String organistion_id);

    @GET("getCompleteDetailofOrganisation")
    Call<ProfileResponseActivity> getCompleteDetailofOrganisation(@Query("organistion_id") String organistion_id);

    @GET("getAvailableCareTakersofOrganisation")
    Call<AvailableCareTakersResponseModel> getAvailableCareTakersofOrganisation(@Query("organistion_id") String organistion_id);

    @GET("fetchDoctorDetailsbyID")
    Call<DoctorLoginResponseModel> fetchDoctorDetailsbyID(@Query("id") String id);

    @GET("getCareTakerCompleteDetails")
    Call<CareTakerCompleteDetailsModel> getCareTakerCompleteDetails(@Query("id") String id);

    @GET("getDoctorsSpecilizations")
    Call<DoctorsSpecilizationModel> getDoctorsSpecilizations();

    @GET("getCompleteDetailsOfProject")
    Call<ProjectCompletDetailsModel> getCompleteDetailsOfProject(@Query("project_id") String project_id);

    @FormUrlEncoded
    @POST("updateOrganisationFcmID")
    Call<RegisterResponseModel> updateOrganisationFcmID(@Field("organisation_id") String organisation_id, @Field("fcm_id") String fcm_id);

    @FormUrlEncoded
    @POST("updateDoctorFcmID")
    Call<RegisterResponseModel> updateDoctorFcmID(@Field("doctor_id") String doctor_id, @Field("fcm_id") String fcm_id);



    @FormUrlEncoded
    @POST("cancelDoctorAppointment")
    Call<RegisterResponseModel> cancelDoctorAppointment(@Field("doctor_id") String doctor_id, @Field("appointment_id") String fcm_id);


    @FormUrlEncoded
    @POST("updateQuoteDetails")
    Call<RegisterResponseModel> updateQuoteDetails(@Field("caretaker_id") String caretaker_id, @Field("coated_price") String coated_price, @Field("quote_id") String quote_id, @Field("oldcareTakerId") String oldcareTakerId);

    @FormUrlEncoded
    @POST("cancelQuote")
    Call<RegisterResponseModel> cancelQuote(@Field("quote_id") String quote_id, @Field("caretaker_id") String caretaker_id);

    @FormUrlEncoded
    @POST("updateProjectCompletion")
    Call<RegisterResponseModel> updateProjectCompletion(@Field("project_id") String project_id);


    @FormUrlEncoded
    @POST("deleteCareTaker")
    Call<RegisterResponseModel> deleteCareTaker(@Field("caretaker_id") String caretaker_id);


    @FormUrlEncoded
    @POST("confimQuote")
    Call<RegisterResponseModel> confimQuote(@Field("project_id") String project_id, @Field("caretaker_id") String cartaker_id, @Field("coated_price") String coated_price);


    @FormUrlEncoded
    @POST("createOrganisation")
    Call<RegisterResponseModel> createOrganisation(@Field("name") String name, @Field("concerned_person") String concerned_person, @Field("email") String email, @Field("mobile") String mobile, @Field("message") String message, @Field("type") String type);

    @Multipart
    @POST("createCareTaker")
    Call<RegisterResponseModel> createCareTaker(@Part MultipartBody.Part file, @Part("name") RequestBody name, @Part("organisation_id") RequestBody organisation_id, @Part("email") RequestBody email, @Part("mobile") RequestBody mobile, @Part("gender") RequestBody gender, @Part("age") RequestBody age, @Part("doj") RequestBody doj, @Part("qualification") RequestBody qualification, @Part("experience") RequestBody experience, @Part("designation") RequestBody designation, @Part("addressproof") RequestBody addressproof, @Part("blood_group") RequestBody blood_group, @Part("martial_status") RequestBody martial_status, @Part("background_verification") RequestBody background_verification, @Part("address") RequestBody address, @Part("residence_number") RequestBody residence_number, @Part("father") RequestBody father, @Part("mother") RequestBody mother, @Part("spouse") RequestBody spouse, @Part("children") RequestBody children, @Part("height") RequestBody height, @Part("weight") RequestBody weight, @Part("languages") RequestBody language);

    @Multipart
    @POST("doctorregister")
    Call<RegisterResponseModel> createDoctor(@Part MultipartBody.Part file, @Part("name") RequestBody name, @Part("location") RequestBody location, @Part("email") RequestBody email, @Part("mobile") RequestBody mobile, @Part("gender") RequestBody gender, @Part("age") RequestBody age, @Part("password") RequestBody password, @Part("latlong") RequestBody latlong, @Part("experience") RequestBody experience, @Part("specialiation") RequestBody specialiation, @Part("additional_degree") RequestBody additional_degree, @Part("time_slots") RequestBody time_slots);


    @Multipart
    @POST("updateAadharCard")
    Call<RegisterResponseModel> updateAadharCard(@Part MultipartBody.Part file, @Part("id") RequestBody id);

    @Multipart
    @POST("updatePoliceCertificate")
    Call<RegisterResponseModel> updatePoliceCertifcateOfCaretaker(@Part MultipartBody.Part file, @Part("id") RequestBody id);

    @Multipart
    @POST("updateExtraCertificate")
    Call<RegisterResponseModel> updateExtraCertificate(@Part MultipartBody.Part file, @Part("id") RequestBody id);

    @Multipart
    @POST("updateCareTakerProfilePicture")
    Call<RegisterResponseModel> updateCareTakerProfilePicture(@Part MultipartBody.Part file, @Part("id") RequestBody id);

    @Multipart
    @POST("updateDoctorProfilePicture")
    Call<RegisterResponseModel> updateDoctorProfilePicture(@Part MultipartBody.Part file, @Part("id") RequestBody id);

    @FormUrlEncoded
    @POST("updateCareTakerDetails")
    Call<UpdateCareTakerDetailsModel> updateCareTakerDetails(@Field("name") String name, @Field("id") String id, @Field("email") String email, @Field("mobile") String mobile, @Field("gender") String gender, @Field("age") String age, @Field("doj") String doj, @Field("qualification") String qualification, @Field("experience") String experience, @Field("designation") String designation, @Field("addressproof") String addressproof, @Field("blood_group") String blood_group, @Field("martial_status") String martial_status, @Field("background_verification") String background_verification, @Field("address") String address, @Field("residence_number") String residence_number, @Field("father") String father, @Field("mother") String mother, @Field("spouse") String spouse, @Field("children") String children);

    @FormUrlEncoded
    @POST("updateDoctorDetails")
    Call<UpdateCareTakerDetailsModel> updateDoctorDetails(@Field("name") String name, @Field("id") String id, @Field("email") String email, @Field("mobile") String mobile, @Field("gender") String gender, @Field("age") String age, @Field("location") String location, @Field("latlong") String latlong, @Field("specialiation") String specialiation, @Field("experience") String experience, @Field("additional_degree") String additional_degree, @Field("time_slots") String time_slots);

}
