package com.grihachikitsa.gcpatner.Activites.Activites;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProfileResponseActivity;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {




    private TextView organisationName,estYear,ongoingProject,completedProjects,email,mobile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }
        elementsIntilization();
        getOrganisationDetails();


    }

    public void elementsIntilization()
    {
        organisationName = (TextView) findViewById(R.id.name);
        estYear = (TextView) findViewById(R.id.estYear);
        ongoingProject = (TextView) findViewById(R.id.onGoingProject);
        completedProjects = (TextView) findViewById(R.id.completedProjects);
        email = (TextView) findViewById(R.id.email);
        mobile = (TextView) findViewById(R.id.mobile);

    }

    public void getOrganisationDetails()
    {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(ProfileActivity.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProfileResponseActivity> call = apiService.getCompleteDetailofOrganisation(Prefs.getString("organisationID", ""));
        call.enqueue(new Callback<ProfileResponseActivity>() {
            @Override
            public void onResponse(Call<ProfileResponseActivity> call, Response<ProfileResponseActivity> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                      customProgressDialog.cancel();
                       printDetails(response.body().getTasks().get(0));
                    } else {
                        customProgressDialog.cancel();
                    }

                } else {
                    customProgressDialog.cancel();

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<ProfileResponseActivity> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();

            }


        });
    }

    public  void printDetails(ProfileResponseActivity profileResponseActivity)
    {
        if(profileResponseActivity!=null)
        {
            organisationName.setText("Organisation: "+checkNull(profileResponseActivity.getName()));
            estYear.setText("Est Year: "+checkNull(profileResponseActivity.getEst_year()));
            email.setText("Email: "+checkNull(profileResponseActivity.getEmail()));
            mobile.setText("Mobile: "+checkNull(profileResponseActivity.getMobile()));
            ongoingProject.setText("Ongoing projects: "+checkNull(profileResponseActivity.getOngoing_projects_count()));
            completedProjects.setText("Completed project: "+checkNull(profileResponseActivity.getCompleted_projects_count()));
        }
    }

    public String checkNull(String s)
    {
        if(s!=null && !s.isEmpty())
        {
            return s;
        }
        else return "N/A" +
                "";
    }
}
