package com.grihachikitsa.gcpatner.Activites.Activites;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.grihachikitsa.gcpatner.Activites.Activites.DoctorsActivites.DoctorRegistrationActivity;
import com.grihachikitsa.gcpatner.Activites.Adapters.CareTakersSpinnerAdapter;
import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetSelectionFragment;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.AvailableCareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.DoctorLoginResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.LoginResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SplashActivity extends AppCompatActivity {

    private Button login, register;
    private RelativeLayout loginLayout;
    private TextView loginRegister;
    private EditText loginEmail, loginPassword;
    private Boolean loginBoolean = false;
    private LinearLayout belowLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        elementsInitilization();
        onClickListeners();
    }


    public void elementsInitilization() {

        //BUTTONS INTILIZATION
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register);

        //RELATIVE LAYOUTS INTILIZATION
        loginLayout = (RelativeLayout) findViewById(R.id.loginlayout);

        //TEXTVIEWS INITILIZATION
        loginRegister = (TextView) findViewById(R.id.loginRegister);

        //LINEAR LAYOUT INTILIZATION
        belowLayout = (LinearLayout) findViewById(R.id.belowlayout);

        //EDITTEXT INTITILIZATION
        loginEmail = (EditText) findViewById(R.id.loginEmail);
        loginPassword = (EditText) findViewById(R.id.loginPassword);
    }

    public void onClickListeners() {


        //BUTTONS ON CLICK LISTENERS
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (loginBoolean) {
                    if (loginEmail.getText().toString().isEmpty() || !isValidEmail(loginEmail.getText().toString())) {
                        Toast.makeText(SplashActivity.this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
                    } else {
                        if (loginPassword.getText().toString().isEmpty()) {
                            Toast.makeText(SplashActivity.this, "Please enter your password", Toast.LENGTH_SHORT).show();
                        } else {

                            List<String> users = new ArrayList<String>();
                            users.add("You are a doctor");
                            users.add("You are a service provider");
                            users.add("You are a medical equipments provide");
                            final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(users, "user_login");
                            myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());


                            // login();
                        }
                    }
                } else {
                    loginBoolean = true;
                    loginLayout.setVisibility(View.VISIBLE);
                    register.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            0, LinearLayout.LayoutParams.MATCH_PARENT);
                    params.weight = 2.0f;
                    login.setLayoutParams(params);
                }

            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(SplashActivity.this, RegisterActivity.class));

                List<String> users = new ArrayList<String>();
                users.add("You are a doctor");
                users.add("You are a service provider");
                users.add("You are a medical equipments provide");
                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(users, "users");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });


        // TEXTVIEW ONCLICK LISTENERS

        loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> users = new ArrayList<String>();
                users.add("You are a doctor");
                users.add("You are a service provider");
                users.add("You are a medical equipments provide");
                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(users, "users");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });


    }

    public void setText(String type, String selectedText) {
        if (type.equalsIgnoreCase("users")) {

            if (selectedText.equalsIgnoreCase("You are a service provider")) {


                startActivity(new Intent(SplashActivity.this, RegisterActivity.class).putExtra("user", "0"));
            } else if (selectedText.equalsIgnoreCase("You are a medical equipments provide")) {
                startActivity(new Intent(SplashActivity.this, RegisterActivity.class).putExtra("user", "1"));
            } else {
                startActivity(new Intent(SplashActivity.this, DoctorRegistrationActivity.class));
            }
        } else if (type.equalsIgnoreCase("user_login")) {
            if (selectedText.equalsIgnoreCase("You are a service provider")) {

                login();

            } else if (selectedText.equalsIgnoreCase("You are a medical equipments provide")) {

            } else {
                doctorLogin();
            }
        }


    }


    public void doctorLogin() {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(SplashActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<DoctorLoginResponseModel> call = apiService.doctorlogin(loginEmail.getText().toString(), loginPassword.getText().toString());
        call.enqueue(new Callback<DoctorLoginResponseModel>() {
            @Override
            public void onResponse(Call<DoctorLoginResponseModel> call, Response<DoctorLoginResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Log.d("Response", response.body().toString() + "");
                    customProgressDialog.cancel();
                    Gson gson = new Gson();
                    Prefs.putString("doctor_details", gson.toJson(response.body()));
                    Prefs.putString("doctorID", response.body().getId());
                    startActivity(new Intent(SplashActivity.this, com.grihachikitsa.gcpatner.Activites.Activites.DoctorsActivites.MainActivity.class));

                } else {
                    customProgressDialog.cancel();
                    Log.d("response", "null response");
                    Toast.makeText(SplashActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DoctorLoginResponseModel> call, Throwable t) {

                t.printStackTrace();
                Toast.makeText(SplashActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                customProgressDialog.cancel();
            }


        });
    }


    public void login() {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(SplashActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<LoginResponseModel> call = apiService.organisationLogin(loginEmail.getText().toString(), loginPassword.getText().toString());
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Log.d("Response", response.body().toString() + "");
                    customProgressDialog.cancel();
                    Gson gson = new Gson();
                    Prefs.putString("user_details", gson.toJson(response.body()));
                    Prefs.putString("organisationID", response.body().getId());
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));

                } else {
                    customProgressDialog.cancel();
                    Log.d("response", "null response");
                    Toast.makeText(SplashActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {

                t.printStackTrace();
                Toast.makeText(SplashActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                customProgressDialog.cancel();
            }


        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!Prefs.getString("organisationID", "").isEmpty()) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            belowLayout.setVisibility(View.GONE);
        } else if (!Prefs.getString("doctorID", "").isEmpty()) {
            belowLayout.setVisibility(View.GONE);
            fetchDoctorDetails();
        } else {
            belowLayout.setVisibility(View.VISIBLE);
        }
    }

    public void fetchDoctorDetails() {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(SplashActivity.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<DoctorLoginResponseModel> call = apiService.fetchDoctorDetailsbyID(Prefs.getString("doctorID", ""));
        call.enqueue(new Callback<DoctorLoginResponseModel>() {
            @Override
            public void onResponse(Call<DoctorLoginResponseModel> call, final Response<DoctorLoginResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    customProgressDialog.cancel();
                    Gson gson = new Gson();
                    Prefs.putString("doctor_details", gson.toJson(response.body()));
                    Prefs.putString("doctorID", response.body().getId());
                    startActivity(new Intent(SplashActivity.this, com.grihachikitsa.gcpatner.Activites.Activites.DoctorsActivites.MainActivity.class));


                } else {
                    customProgressDialog.cancel();
                    Toast.makeText(SplashActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<DoctorLoginResponseModel> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();
                Toast.makeText(SplashActivity.this, "something went wrong", Toast.LENGTH_SHORT).show();

            }


        });
    }
}
