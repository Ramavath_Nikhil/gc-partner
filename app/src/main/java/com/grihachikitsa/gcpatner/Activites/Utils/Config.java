package com.grihachikitsa.gcpatner.Activites.Utils;

/**
 * Created by Nikil on 12/27/2016.
 */
public class Config {



    public static final String BASE_URL = "http://www.getmydish.com/dodo/dodoserver/v1/";
    // public static final String BASE_URL = "http://192.168.43.131/gc/v1/";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;
    public static final String ORGANISATION_MOBILE_NUMBER = "tel:9739176107";
}
