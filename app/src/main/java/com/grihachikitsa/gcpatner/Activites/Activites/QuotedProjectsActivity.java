package com.grihachikitsa.gcpatner.Activites.Activites;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.grihachikitsa.gcpatner.Activites.Adapters.CareTakersAdapter;
import com.grihachikitsa.gcpatner.Activites.Adapters.QuotesAdapter;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.QuotesModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;

public class QuotedProjectsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private QuotesAdapter mAdapter;
    private List<QuotesModel> careTakersResponseModels = new ArrayList<>();
    private ErrorView errorView;
    private SwipeRefreshLayout swipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quoted_projects);
        if (getSupportActionBar() != null) {
            android.support.v7.app.ActionBar bar = getSupportActionBar();
            bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        elementsInilization();
        getOngoingProjectsData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void elementsInilization() {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(QuotedProjectsActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new QuotesAdapter(careTakersResponseModels, QuotedProjectsActivity.this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);

        //ERRORVIEW INTITLIZATION
        errorView = (ErrorView) findViewById(R.id.error_view);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                errorView.setVisibility(View.GONE);
                getOngoingProjectsData();
            }
        });

        //SWIPE REFRESH LAYOUT INTILIZATION
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefreshkayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getOngoingProjectsData();
            }
        });

    }

    public void getOngoingProjectsData() {
        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<QuotesModel> call = apiService.getQuotedProjectsByOrganisation(Prefs.getString("organisationID", ""));
        call.enqueue(new Callback<QuotesModel>() {
            @Override
            public void onResponse(Call<QuotesModel> call, Response<QuotesModel> response) {
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {




                    if (careTakersResponseModels != null && careTakersResponseModels.size() > 0) {
                        careTakersResponseModels.clear();
                        mAdapter.notifyDataSetChanged();
                    }


                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        errorView.setVisibility(View.GONE);
                        careTakersResponseModels.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        progressBar.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        errorView.setTitle("No Projects Available");
                        errorView.setSubtitle("Please retry");
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    errorView.setSubtitle("Please retry");
                    recyclerView.setVisibility(View.GONE);
                    errorView.setTitle("Something went wrong");

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<QuotesModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                errorView.setTitle("Something went wrong");

            }


        });
    }
}
