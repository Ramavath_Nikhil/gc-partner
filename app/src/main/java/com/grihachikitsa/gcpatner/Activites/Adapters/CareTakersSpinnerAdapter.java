package com.grihachikitsa.gcpatner.Activites.Adapters;

/**
 * Created by Nikil on 12/30/2016.
 */

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.AvailableCareTakersResponseModel;
import com.grihachikitsa.gcpatner.R;

/*****
 * Adapter class extends with ArrayAdapter
 ******/
public class CareTakersSpinnerAdapter extends ArrayAdapter<String> {

    private Context activity;
    private List<AvailableCareTakersResponseModel> data;
    public Resources res;
    AvailableCareTakersResponseModel tempValues = null;
    LayoutInflater inflater;

    /*************
     * CustomAdapter Constructor
     *****************/
    public CareTakersSpinnerAdapter(
            Context activitySpinner,
            int textViewResourceId,
            ArrayList objects,
            Resources resLocal
    ) {
        super(activitySpinner, textViewResourceId, objects);

        /********** Take passed values **********/
        activity = activitySpinner;
        data = objects;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.list_item_availablecaretaker, parent, false);

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (AvailableCareTakersResponseModel) data.get(position);

        TextView label = (TextView) row.findViewById(R.id.careTakerName);
        label.setText(tempValues.getCaretaker_name());


        return row;
    }
}
