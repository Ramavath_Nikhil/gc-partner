package com.grihachikitsa.gcpatner.Activites.Fragments;

/**
 * Created by Nikil on 12/6/2016.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.grihachikitsa.gcpatner.Activites.Adapters.NewLeadsAdapter;
import com.grihachikitsa.gcpatner.Activites.Models.NewLeadsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.NewLeadsResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProfileResponseActivity;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tr.xip.errorview.ErrorView;


public class NewLeadsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private NewLeadsAdapter mAdapter;
    private List<NewLeadsResponseModel> newLeadsResponseModels = new ArrayList<>();
    private ErrorView errorView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public NewLeadsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.layout_fragment_newleads, container, false);
        elementsInilization(view);
        getOngoingProjectsData();
        return view;
    }


    public void elementsInilization(View view) {
        //RECYCLERVIEW INTILIAZTION
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NewLeadsAdapter(newLeadsResponseModels, getActivity(), this);
        recyclerView.setAdapter(mAdapter);


        //PROGRESS BAR INITLIZATION
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

        //ERRORVIEW INTITLIZATION
        errorView = (ErrorView) view.findViewById(R.id.error_view);
        errorView.setOnRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                errorView.setVisibility(View.GONE);
                getOngoingProjectsData();
            }
        });

        //SWIPE REFRESH LAYOUT INTILIZATION
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefreshkayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary),
               getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                errorView.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(true);
                getOngoingProjectsData();
            }
        });

    }

    public void getOngoingProjectsData() {

        swipeRefreshLayout.setRefreshing(false);
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<NewLeadsResponseModel> call = apiService.getPendingProjectForOrganisation(Prefs.getString("organisationID", ""));
        call.enqueue(new Callback<NewLeadsResponseModel>() {
            @Override
            public void onResponse(Call<NewLeadsResponseModel> call, Response<NewLeadsResponseModel> response) {

                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (newLeadsResponseModels != null && newLeadsResponseModels.size() > 0) {
                        newLeadsResponseModels.clear();
                        mAdapter.notifyDataSetChanged();
                    }

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {

                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        errorView.setVisibility(View.GONE);
                        newLeadsResponseModels.addAll(response.body().getTasks());
                        mAdapter.notifyDataSetChanged();
                    } else {
                        errorView.setTitle("No new leads available");
                        errorView.setSubtitle("Please retry after sometime");
                        progressBar.setVisibility(View.GONE);
                        errorView.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }

                } else {
                    progressBar.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    errorView.setTitle("Something went wrong");
                    Log.d("response", "null response");
                    errorView.setSubtitle("Please retry after sometime");
                }
            }

            @Override
            public void onFailure(Call<NewLeadsResponseModel> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                errorView.setTitle("Something went wrong");
                errorView.setSubtitle("Please retry after sometime");

            }


        });


    }


}

