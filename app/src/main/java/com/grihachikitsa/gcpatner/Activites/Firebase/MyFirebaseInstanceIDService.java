package com.grihachikitsa.gcpatner.Activites.Firebase;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Config;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
        FirebaseMessaging.getInstance().subscribeToTopic("organisations");
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        Prefs.putString("regId", token);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


            Call<RegisterResponseModel> call = apiService.updateOrganisationFcmID(Prefs.getString("organisationID",""),token);
            call.enqueue(new Callback<RegisterResponseModel>() {
                @Override
                public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                    if(response.body()!=null)
                    {
                        if(response.body().getError()!=null && !response.body().getError().isEmpty())
                            Log.d("fcm_update","updated");
                        else
                            Log.d("fcm_update","not updated");
                    }
                    else
                    {
                        Log.d("fcm_update","response body null");
                    }
                }

                @Override
                public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                }
            });



    }

    private void storeRegIdInPref(String token) {
        Prefs.putString("c", token);
    }
}