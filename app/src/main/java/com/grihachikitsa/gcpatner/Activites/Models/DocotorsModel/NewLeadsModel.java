package com.grihachikitsa.gcpatner.Activites.Models.DocotorsModel;

import java.util.List;

/**
 * Created by Nikil on 3/18/2017.
 */
public class NewLeadsModel {


    String error,

    id,
            doctor_id,
            user_id,
            date,
            timeslot,
            timeslot_sub,
            created_at,
            status,
            latlong,
            address,
            user_name;

    private List<NewLeadsModel> tasks;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getTimeslot_sub() {
        return timeslot_sub;
    }

    public void setTimeslot_sub(String timeslot_sub) {
        this.timeslot_sub = timeslot_sub;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public List<NewLeadsModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<NewLeadsModel> tasks) {
        this.tasks = tasks;
    }
}
