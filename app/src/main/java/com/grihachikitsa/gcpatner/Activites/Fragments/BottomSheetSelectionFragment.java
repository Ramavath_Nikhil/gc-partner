package com.grihachikitsa.gcpatner.Activites.Fragments;

/**
 * Created by nikhi on 10/20/2016.
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;


import com.grihachikitsa.gcpatner.Activites.Activites.AddCareTaker;
import com.grihachikitsa.gcpatner.Activites.Activites.DoctorsActivites.DoctorRegistrationActivity;
import com.grihachikitsa.gcpatner.Activites.Activites.DoctorsActivites.Doctor_profile_edit;
import com.grihachikitsa.gcpatner.Activites.Activites.SplashActivity;
import com.grihachikitsa.gcpatner.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by MG on 17-07-2016.
 */
public class BottomSheetSelectionFragment extends BottomSheetDialogFragment {


    private static BottomSheetBehavior mBottomSheetBehavior;
    public List<String> data, timeslots;
    public String type;

    public static BottomSheetSelectionFragment newInstance(List<String> data, String type) {
        BottomSheetSelectionFragment f = new BottomSheetSelectionFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("data", (ArrayList<String>) data);
        args.putString("type", type);
        f.setArguments(args);

        return f;
    }

    public static BottomSheetSelectionFragment newInstance(List<String> data, String type, String[] timeslots) {
        BottomSheetSelectionFragment f = new BottomSheetSelectionFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("data", (ArrayList<String>) data);
        args.putString("type", type);

        ArrayList<String> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, timeslots);
        args.putStringArrayList("timeslots", arrayList);


        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        data = getArguments().getStringArrayList("data");
        type = getArguments().getString("type");

    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        final View v = View.inflate(getContext(), R.layout.layout_fragment_bottomsheet_selectionfragment, null);
        dialog.setContentView(v);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) v.getParent()).getLayoutParams();
        final Button save = (Button) v.findViewById(R.id.save);
        final RecyclerView mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);


        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {


                    if (newState == 5)
                        getDialog().cancel();
                    if (type.equalsIgnoreCase("timeslots")) {
                        String selectedItems = "";
                        for (int i = 0; i < data.size(); i++) {
                            AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                            if (tv.isChecked()) {

                                selectedItems = selectedItems + tv.getText() + ",";

                            }


                        }

                        if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length() - 1) == ',') {
                            selectedItems = selectedItems.substring(0, selectedItems.length() - 1);
                        }
                        ((DoctorRegistrationActivity) getActivity()).setText("timeslots", selectedItems);
                    }
                    else if (type.equalsIgnoreCase("timeslots_edit"))
                    {
                        String selectedItems = "";
                        for (int i = 0; i < data.size(); i++) {
                            AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                            if (tv.isChecked()) {

                                selectedItems = selectedItems + tv.getText() + ",";

                            }


                        }

                        if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length() - 1) == ',') {
                            selectedItems = selectedItems.substring(0, selectedItems.length() - 1);
                        }
                        ((Doctor_profile_edit) getActivity()).setText("timeslots", selectedItems);
                    }


                    else if (type.equalsIgnoreCase("language")) {
                        String selectedItems = "";
                        for (int i = 0; i < data.size(); i++) {
                            AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                            if (tv.isChecked()) {

                                selectedItems = selectedItems + tv.getText() + ",";

                            }


                        }

                        if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length() - 1) == ',') {
                            selectedItems = selectedItems.substring(0, selectedItems.length() - 1);
                        }
                        ((AddCareTaker) getActivity()).setText("language", selectedItems);
                    }

                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });

            if (type.equalsIgnoreCase("timeslots") || type.equalsIgnoreCase("language") ||type.equalsIgnoreCase("timeslots_edit")) {
                v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int height = v.getMeasuredHeight();
                        mBottomSheetBehavior.setPeekHeight(height);
                        save.setVisibility(View.VISIBLE);

                    }
                });
            } else if (type.equalsIgnoreCase("users")) {
                mBottomSheetBehavior.setPeekHeight(600);
                save.setVisibility(View.GONE);
            } else if (type.equalsIgnoreCase("doctorgender") || type.equalsIgnoreCase("doctorgender_edit")) {
                mBottomSheetBehavior.setPeekHeight(300);
                save.setVisibility(View.GONE);
            } else {
                save.setVisibility(View.GONE);
                v.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        v.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int height = v.getMeasuredHeight();
                        mBottomSheetBehavior.setPeekHeight(height);


                    }
                });
            }

            save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getDialog().cancel();
                    if (type.equalsIgnoreCase("timeslots")) {
                        String selectedItems = "";
                        for (int i = 0; i < data.size(); i++) {
                            AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                            if (tv.isChecked()) {

                                selectedItems = selectedItems + tv.getText() + ",";

                            }


                        }

                        if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length() - 1) == ',') {
                            selectedItems = selectedItems.substring(0, selectedItems.length() - 1);
                        }
                        ((DoctorRegistrationActivity) getActivity()).setText("timeslots", selectedItems);
                    }

                    else if (type.equalsIgnoreCase("timeslots_edit"))
                    {
                        String selectedItems = "";
                        for (int i = 0; i < data.size(); i++) {
                            AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                            if (tv.isChecked()) {

                                selectedItems = selectedItems + tv.getText() + ",";

                            }


                        }

                        if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length() - 1) == ',') {
                            selectedItems = selectedItems.substring(0, selectedItems.length() - 1);
                        }
                        ((Doctor_profile_edit) getActivity()).setText("timeslots", selectedItems);
                    }


                    else if (type.equalsIgnoreCase("language")) {
                        String selectedItems = "";
                        for (int i = 0; i < data.size(); i++) {
                            AppCompatCheckedTextView tv = (AppCompatCheckedTextView) mRecyclerView.getChildAt(i);
                            if (tv.isChecked()) {

                                selectedItems = selectedItems + tv.getText() + ",";

                            }


                        }

                        if (selectedItems != null && selectedItems.length() > 0 && selectedItems.charAt(selectedItems.length() - 1) == ',') {
                            selectedItems = selectedItems.substring(0, selectedItems.length() - 1);
                        }
                        ((AddCareTaker) getActivity()).setText("language", selectedItems);
                    }


                }
            });
        }


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(new RecyclerView.Adapter<PlanetViewHolder>() {

            @Override
            public PlanetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

                if (!type.equalsIgnoreCase("timeslots") && !type.equalsIgnoreCase("language") && !type.equalsIgnoreCase("timeslots_edit")) {
                    View v = LayoutInflater.from(parent.getContext()).inflate(
                            android.R.layout.simple_list_item_1,
                            parent,
                            false);
                    PlanetViewHolder vh = new PlanetViewHolder(v);
                    return vh;
                } else {
                    View v = LayoutInflater.from(parent.getContext()).inflate(
                            android.R.layout.simple_list_item_multiple_choice,
                            parent,
                            false);
                    PlanetViewHolder vh = new PlanetViewHolder(v);
                    return vh;
                }

            }

            @Override
            public void onBindViewHolder(PlanetViewHolder vh, int position) {

                if (!type.equalsIgnoreCase("timeslots") && !type.equalsIgnoreCase("language") &&!type.equalsIgnoreCase("timeslots_edit")) {
                    TextView tv = (TextView) vh.itemView;
                    tv.setText(data.get(position));
                } else {
                    AppCompatCheckedTextView tv = (AppCompatCheckedTextView) vh.itemView;
                    tv.setText(data.get(position));
                    if (timeslots != null && timeslots.size() > 0 && timeslots.contains(data.get(position)))
                        tv.setChecked(true);
                    else
                        tv.setChecked(false);
                }


            }

            @Override
            public int getItemCount() {
                return data.size();
            }
        });


    }


    private class PlanetViewHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public PlanetViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            if (!type.equalsIgnoreCase("timeslots") && !type.equalsIgnoreCase("language") && !type.equalsIgnoreCase("timeslots_edit")) {
                getDialog().cancel();
                if (type.equalsIgnoreCase("gender")) {
                    ((AddCareTaker) getActivity()).setText("gender", ((TextView) v).getText().toString());

                } else if (type.equalsIgnoreCase("backgroundVerification")) {
                    ((AddCareTaker) getActivity()).setText("backgroundVerification", ((TextView) v).getText().toString());

                } else if (type.equalsIgnoreCase("martialStatus")) {

                    ((AddCareTaker) getActivity()).setText("martialStatus", ((TextView) v).getText().toString());
                } else if (type.equalsIgnoreCase("bloodGroup")) {

                    ((AddCareTaker) getActivity()).setText("bloodGroup", ((TextView) v).getText().toString());
                } else if (type.equalsIgnoreCase("users")) {
                    ((SplashActivity) getActivity()).setText("users", ((TextView) v).getText().toString());
                } else if (type.equalsIgnoreCase("user_login")) {
                    ((SplashActivity) getActivity()).setText("user_login", ((TextView) v).getText().toString());
                } else if (type.equalsIgnoreCase("doctorgender")) {
                    ((DoctorRegistrationActivity) getActivity()).setText("doctorgender", ((TextView) v).getText().toString());
                }

                else if (type.equalsIgnoreCase("doctorgender_edit")) {
                    ((Doctor_profile_edit) getActivity()).setText("doctorgender", ((TextView) v).getText().toString());
                }

                else if (type.equalsIgnoreCase("specialztion")) {
                    ((DoctorRegistrationActivity) getActivity()).setText("specialztion", ((TextView) v).getText().toString());
                }

                else if (type.equalsIgnoreCase("specialztion_edit")) {
                    ((Doctor_profile_edit) getActivity()).setText("specialztion", ((TextView) v).getText().toString());
                }

                else if (type.equalsIgnoreCase("height")) {
                    ((AddCareTaker) getActivity()).setText("height", ((TextView) v).getText().toString());
                } else if (type.equalsIgnoreCase("weight")) {
                    ((AddCareTaker) getActivity()).setText("weight", ((TextView) v).getText().toString());
                } else if (type.equalsIgnoreCase("designation")) {
                    ((AddCareTaker) getActivity()).setText("designation", ((TextView) v).getText().toString());
                }

            } else {
                AppCompatCheckedTextView checkedTextView = ((AppCompatCheckedTextView) v);


                if (checkedTextView.isChecked()) {
                    checkedTextView.setChecked(false);
                } else {
                    checkedTextView.setChecked(true);
                }


            }

        }
    }


}