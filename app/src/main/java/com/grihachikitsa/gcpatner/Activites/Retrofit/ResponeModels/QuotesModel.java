package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

import java.util.List;

/**
 * Created by Nikil on 1/12/2017.
 */
public class QuotesModel {




    private String id;
    private String type;
    private String patient_name;
    private String user_id;
    private String gender;
    private String age;
    private String height;
    private String weight;
    private String services;
    private String hours;
    private String duration;
    private String moreinfo;
    private String created_at;
    private String status;
    private String latlong;
    private String address;
    private String caretaker_id;
    private String coated_price;
    private String coated_on;
    private String caretaker_name;
    private String projectquote_id;

    public String getProject_caretaker_id() {
        return project_caretaker_id;
    }

    public void setProject_caretaker_id(String project_caretaker_id) {
        this.project_caretaker_id = project_caretaker_id;
    }

    private String project_caretaker_id;

    public String getProject_coated_on() {
        return project_coated_on;
    }

    public void setProject_coated_on(String project_coated_on) {
        this.project_coated_on = project_coated_on;
    }

    private String project_coated_on;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String error;

    public String getProjectquotes_coatedprice() {
        return projectquotes_coatedprice;
    }

    public void setProjectquotes_coatedprice(String projectquotes_coatedprice) {
        this.projectquotes_coatedprice = projectquotes_coatedprice;
    }

    private String projectquotes_coatedprice;
    private List<QuotesModel> tasks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getMoreinfo() {
        return moreinfo;
    }

    public void setMoreinfo(String moreinfo) {
        this.moreinfo = moreinfo;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLatlong() {
        return latlong;
    }

    public void setLatlong(String latlong) {
        this.latlong = latlong;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCaretaker_id() {
        return caretaker_id;
    }

    public void setCaretaker_id(String caretaker_id) {
        this.caretaker_id = caretaker_id;
    }

    public String getCoated_price() {
        return coated_price;
    }

    public void setCoated_price(String coated_price) {
        this.coated_price = coated_price;
    }

    public String getCoated_on() {
        return coated_on;
    }

    public void setCoated_on(String coated_on) {
        this.coated_on = coated_on;
    }

    public String getCaretaker_name() {
        return caretaker_name;
    }

    public void setCaretaker_name(String caretaker_name) {
        this.caretaker_name = caretaker_name;
    }

    public String getProjectquote_id() {
        return projectquote_id;
    }

    public void setProjectquote_id(String projectquote_id) {
        this.projectquote_id = projectquote_id;
    }

    public List<QuotesModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<QuotesModel> tasks) {
        this.tasks = tasks;
    }
}
