package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

import java.util.List;

/**
 * Created by Nikil on 12/29/2016.
 */
public class ProfileResponseActivity {



    private String id;
    private String name;
    private String concerened_person;
    private String email;
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String password;
    private String mobile;
    private String message;
    private String status;
    private String created_at;
    private String profile_pic;
    private String est_year;
    private String moreinfo;
    private String ongoing_projects_count;
    private String completed_projects_count;
    private List<ProfileResponseActivity> tasks;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConcerened_person() {
        return concerened_person;
    }

    public void setConcerened_person(String concerened_person) {
        this.concerened_person = concerened_person;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getEst_year() {
        return est_year;
    }

    public void setEst_year(String est_year) {
        this.est_year = est_year;
    }

    public String getMoreinfo() {
        return moreinfo;
    }

    public void setMoreinfo(String moreinfo) {
        this.moreinfo = moreinfo;
    }

    public String getOngoing_projects_count() {
        return ongoing_projects_count;
    }

    public void setOngoing_projects_count(String ongoing_projects_count) {
        this.ongoing_projects_count = ongoing_projects_count;
    }

    public String getCompleted_projects_count() {
        return completed_projects_count;
    }

    public void setCompleted_projects_count(String completed_projects_count) {
        this.completed_projects_count = completed_projects_count;
    }

    public List<ProfileResponseActivity> getTasks() {
        return tasks;
    }

    public void setTasks(List<ProfileResponseActivity> tasks) {
        this.tasks = tasks;
    }
}
