package com.grihachikitsa.gcpatner.Activites.Activites;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;


import com.google.firebase.messaging.FirebaseMessaging;
import com.grihachikitsa.gcpatner.Activites.Fragments.CompletedProjectsFragment;
import com.grihachikitsa.gcpatner.Activites.Fragments.NewLeadsFragment;
import com.grihachikitsa.gcpatner.Activites.Fragments.OngoingProjectFragment;
import com.grihachikitsa.gcpatner.Activites.Fragments.ProfileFragment;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Config;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    private ActionBarDrawerToggle mDrawerToggle;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private static final int CALL_REQUEST = 501;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        elementInitilization();

    }

    public void elementInitilization()
    {
        //NAVIGATION DRAWER INTILIZATION
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();

                if (menuItem.getItemId() == R.id.addCareTaker) {
                    startActivity(new Intent(MainActivity.this, AddCareTaker.class));
                }
                if(menuItem.getItemId() == R.id.editCareTakers)
                {
                    startActivity(new Intent(MainActivity.this,EditCareTaker.class));
                }

                if(menuItem.getItemId() == R.id.account)
                {
                    startActivity(new Intent(MainActivity.this,ProfileActivity.class));
                }

                if(menuItem.getItemId() == R.id.quotes)
                {
                    startActivity(new Intent(MainActivity.this,QuotedProjectsActivity.class));
                }
                if(menuItem.getItemId() == R.id.logout)
                {
                   logout();
                }


                return false;
            }

        });


        //  VIEW PAGER AND TAB LAYOUT INTILIZATION
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);

    }


    public void logout()
    {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_logout);
        Button logout = (Button) dialog.findViewById(R.id.logout);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                startActivity(new Intent(MainActivity.this, SplashActivity.class));
                Prefs.putString("user_details", "");
                Prefs.putString("organisationID", "");
                finish();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.cancel();


            }
        });

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }



    @Override
    protected void onResume() {
        super.onResume();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                   sendRegistrationToServer(Prefs.getString("regId",""));

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();


                }
            }
        };

        sendRegistrationToServer(Prefs.getString("regId",""));
    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server

        Prefs.putString("regId", token);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.updateOrganisationFcmID(Prefs.getString("organisationID",""),token);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                if(response.body()!=null)
                {
                    if(response.body().getError()!=null && !response.body().getError().isEmpty())
                        Log.d("fcm_update","updated");
                    else
                        Log.d("fcm_update","not updated");
                }
                else
                {
                    Log.d("fcm_update","response body null");
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

            }
        });



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_call) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        CALL_REQUEST);
                return false;
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse(Config.ORGANISATION_MOBILE_NUMBER));
                startActivity(callIntent);
            }

        }

        else if ( item.getItemId() == android.R.id.home) {
            // API 5+ solution
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {



            case CALL_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(Config.ORGANISATION_MOBILE_NUMBER));
                    startActivity(callIntent);

                } else {

                    Toast.makeText(MainActivity.this, "Please provide reqiured permission to make a call", Toast.LENGTH_SHORT).show();
                }


                break;


        }

    }



    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new NewLeadsFragment(), "New Leads");
        adapter.addFragment(new OngoingProjectFragment(), "On going");
        adapter.addFragment(new CompletedProjectsFragment(), "Completed");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

