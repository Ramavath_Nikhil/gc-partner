package com.grihachikitsa.gcpatner.Activites.Activites.DoctorsActivites;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.DoctorLoginResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Config;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {

    private EditText name, age, gender, experience, location, specialztion, additionalDegree, timeSlots, email, mobile, password, confrimPassword;
    private Button createAccount;
    private FloatingActionButton editDoctorProfile;
    private CircleImageView profilePic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }



    }

    @Override
    protected void onResume() {
        super.onResume();
        elementsIntilization();
        onClickListeners();
        printDoctorDetails();
    }

    public void elementsIntilization() {
        //EDITTEXT INTILIZATION
        name = (EditText) findViewById(R.id.name);
        age = (EditText) findViewById(R.id.age);
        gender = (EditText) findViewById(R.id.gender);
        experience = (EditText) findViewById(R.id.experience);
        location = (EditText) findViewById(R.id.location);
        specialztion = (EditText) findViewById(R.id.specialization);
        additionalDegree = (EditText) findViewById(R.id.additionalDegree);
        timeSlots = (EditText) findViewById(R.id.timeslots);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);
        confrimPassword = (EditText) findViewById(R.id.confirmPassword);

        //BUTTON INTILIZATION
        createAccount = (Button) findViewById(R.id.updateAccount);

        //FLOATING ACTION BUTTON INTILIZATION
        editDoctorProfile = (FloatingActionButton) findViewById(R.id.editDoctorProfile);

        //CIRCLE IMAGEVIEW INTILIZATION
        profilePic = (CircleImageView) findViewById(R.id.profilepic);
    }

    public void onClickListeners() {
        editDoctorProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this, Doctor_profile_edit.class));
            }
        });
    }

    public void printDoctorDetails() {
        Gson gson = new Gson();
        DoctorLoginResponseModel doctorLoginResponseModel = gson.fromJson(Prefs.getString("doctor_details", ""), DoctorLoginResponseModel.class);
        if (doctorLoginResponseModel != null) {
            name.setText(checkNull(doctorLoginResponseModel.getName()));
            age.setText(checkNull(doctorLoginResponseModel.getAge()));
            gender.setText(checkNull(doctorLoginResponseModel.getGender()));
            experience.setText(checkNull(doctorLoginResponseModel.getExperience()));
            location.setText(checkNull(doctorLoginResponseModel.getLocation()));
            specialztion.setText(checkNull(doctorLoginResponseModel.getSpecialization()));
            additionalDegree.setText(checkNull(doctorLoginResponseModel.getAdditional_degree()));
            timeSlots.setText(checkNull(doctorLoginResponseModel.getTimeslots()));
            email.setText(checkNull(doctorLoginResponseModel.getEmail()));
            mobile.setText(checkNull(doctorLoginResponseModel.getMobile()));

            if (doctorLoginResponseModel.getProfile_pic() != null && !doctorLoginResponseModel.getProfile_pic().isEmpty()) {
                Picasso.with(ProfileActivity.this).load(Config.BASE_URL + doctorLoginResponseModel.getProfile_pic()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person).into(profilePic);

            }
        }
    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

}
