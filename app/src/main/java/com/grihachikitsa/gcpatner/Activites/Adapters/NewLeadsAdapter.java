package com.grihachikitsa.gcpatner.Activites.Adapters;

/**
 * Created by Nikil on 12/8/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.grihachikitsa.gcpatner.Activites.Fragments.NewLeadsFragment;
import com.grihachikitsa.gcpatner.Activites.Models.NewLeadsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.AvailableCareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.NewLeadsResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProfileResponseActivity;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProjectCompletDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.UpdateCareTakerDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewLeadsAdapter extends RecyclerView.Adapter<NewLeadsAdapter.MyViewHolder> {

    private List<NewLeadsResponseModel> newLeadsResponseModels;
    private Context context;
    private NewLeadsFragment newLeadsFragment;
    private String selectedCaretakerID = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, age, weight, gender, services, timePeriod, address,preferedLanguages;
        public Button quote, confirmQuote;
        public Spinner careTaker;
        public EditText quotedPrice;
        public LinearLayout mainlayout;

        public MyViewHolder(View view) {
            super(view);
            //careType = (TextView) view.findViewById(R.id.careType);
            name = (TextView) view.findViewById(R.id.name);
            age = (TextView) view.findViewById(R.id.age);
            gender = (TextView) view.findViewById(R.id.gender);
            services = (TextView) view.findViewById(R.id.serivces);
            timePeriod = (TextView) view.findViewById(R.id.time);
            address = (TextView) view.findViewById(R.id.address);
            weight = (TextView) view.findViewById(R.id.weight);
            quote = (Button) view.findViewById(R.id.quote);
            careTaker = (Spinner) view.findViewById(R.id.careTaker);
            quotedPrice = (EditText) view.findViewById(R.id.quotedPrice);
            confirmQuote = (Button) view.findViewById(R.id.confirmQuote);
            mainlayout = (LinearLayout) view.findViewById(R.id.mainlayout);
            preferedLanguages = (TextView) view.findViewById(R.id.preferedLanguage);
        }
    }


    public NewLeadsAdapter(List<NewLeadsResponseModel> newLeadsResponseModels, Context context, NewLeadsFragment newLeadsFragment) {
        this.newLeadsResponseModels = newLeadsResponseModels;
        this.context = context;
        this.newLeadsFragment = newLeadsFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_newleads, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final NewLeadsResponseModel newLeadsResponseModel = newLeadsResponseModels.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());
        holder.name.setText(checkNull(newLeadsResponseModel.getFirst_name()) + " " + checkNull(newLeadsResponseModel.getLast_name()));
        holder.address.setText("Address: " + checkNullNA(newLeadsResponseModel.getAddress()));
        holder.timePeriod.setText("Time Period: " + checkNullNA(newLeadsResponseModel.getDuration()));
        holder.services.setText("Services: " + checkNullNA(newLeadsResponseModel.getServices()));
        holder.age.setText("Age: " + checkNullNA(newLeadsResponseModel.getAge()));
        holder.weight.setText("Weight: " + checkNullNA(newLeadsResponseModel.getWeight()));
        holder.preferedLanguages.setText("Prefered Languages: "+checkNullNA(newLeadsResponseModel.getFirst_language()));
        if(newLeadsResponseModel.getSecond_language()!=null && !newLeadsResponseModel.getSecond_language().isEmpty())
        {
            holder.preferedLanguages.setText("Prefered Languages: "+checkNullNA(newLeadsResponseModel.getFirst_language() +","+newLeadsResponseModel.getSecond_language()));
        }
        if(newLeadsResponseModel.getThird_language()!=null && !newLeadsResponseModel.getThird_language().isEmpty())
        {
            holder.preferedLanguages.setText("Prefered Languages: "+checkNullNA(newLeadsResponseModel.getFirst_language() +","+newLeadsResponseModel.getSecond_language()+","+newLeadsResponseModel.getThird_language()));
        }
        holder.quote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAvailableCareTakers(newLeadsResponseModel.getProject_id(), holder.careTaker, holder.quotedPrice, holder.confirmQuote, holder.quote);
            }
        });
        holder.mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getProjectDetails(newLeadsResponseModel.getProject_id());
            }
        });


    }

    public void getProjectDetails(String project_id) {


        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProjectCompletDetailsModel> call = apiService.getCompleteDetailsOfProject(project_id);
        call.enqueue(new Callback<ProjectCompletDetailsModel>() {
            @Override
            public void onResponse(Call<ProjectCompletDetailsModel> call, Response<ProjectCompletDetailsModel> response) {
                customProgressDialog.cancel();
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false") && response.body().getTasks() != null && response.body().getTasks().size() > 0 && response.body().getTasks().get(0) != null) {
                    ProjectCompletDetailsModel projectCompletDetailsModel = response.body().getTasks().get(0);

                    printProjectDetails(projectCompletDetailsModel);

                } else {

                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProjectCompletDetailsModel> call, Throwable t) {

                customProgressDialog.cancel();

                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void printProjectDetails(ProjectCompletDetailsModel projectCompletDetailsModel) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_complete_details_of_project);


        // set the custom dialog components - text, image and button


        TextView patientName, patientAge, patientHeight, patientWeight, patientGender, patientServices, hours, duration, moreinfo, address;
        TextView quotes, careTakerName, organisationName, coatedPrice, acceptedOn;
        Button viewQuotes, careTakerDetails, organisationDetails;
        LinearLayout careTakerDetailsLayout;
        TextView careTakerDetailsTitle;


        //TEXTVIEW INTILIZATION
        patientName = (TextView) dialog.findViewById(R.id.name);
        patientAge = (TextView) dialog.findViewById(R.id.age);
        patientHeight = (TextView) dialog.findViewById(R.id.height);
        patientWeight = (TextView) dialog.findViewById(R.id.weight);
        patientGender = (TextView) dialog.findViewById(R.id.gender);
        patientServices = (TextView) dialog.findViewById(R.id.serivces);
        hours = (TextView) dialog.findViewById(R.id.hours);
        duration = (TextView) dialog.findViewById(R.id.duration);
        moreinfo = (TextView) dialog.findViewById(R.id.moreinfo);
        address = (TextView) dialog.findViewById(R.id.address);
        quotes = (TextView) dialog.findViewById(R.id.quotes);
        careTakerName = (TextView) dialog.findViewById(R.id.careTakerName);
        organisationName = (TextView) dialog.findViewById(R.id.organisationName);
        coatedPrice = (TextView) dialog.findViewById(R.id.coatedPrice);
        acceptedOn = (TextView) dialog.findViewById(R.id.coatedOn);
        careTakerDetailsTitle = (TextView) dialog.findViewById(R.id.careTakerDetails);

        //BUTTON INTILIZATION
        viewQuotes = (Button) dialog.findViewById(R.id.viewQuotes);
        careTakerDetails = (Button) dialog.findViewById(R.id.careTakerCompleteDetails);
        organisationDetails = (Button) dialog.findViewById(R.id.organisationCompleteDetails);

        //LINEAR LAYOUT INTILIZATION
        careTakerDetailsLayout = (LinearLayout) dialog.findViewById(R.id.careTakerDetailsLayout);


        patientName.setText("Patient Name: " + checkNull(projectCompletDetailsModel.getPatient_name()));
        patientAge.setText("Age: " + checkNull(projectCompletDetailsModel.getAge()));
        patientGender.setText("Gender: " + checkNull(projectCompletDetailsModel.getGender()));
        patientHeight.setText("Height: " + checkNull(projectCompletDetailsModel.getHeight()));
        patientWeight.setText("Weight: " + checkNull(projectCompletDetailsModel.getWeight()));
        patientServices.setText("Services: " + checkNull(projectCompletDetailsModel.getServices()));
        hours.setText("Time Period: " + checkNull(projectCompletDetailsModel.getHours()));
        duration.setText("Duration: " + checkNull(projectCompletDetailsModel.getDuration()));
        if (projectCompletDetailsModel.getMoreinfo() != null && !projectCompletDetailsModel.getMoreinfo().isEmpty()) {
            moreinfo.setVisibility(View.VISIBLE);
            moreinfo.setText("More info: " + checkNull(projectCompletDetailsModel.getMoreinfo()));
        } else {
            moreinfo.setVisibility(View.GONE);
        }

        if (projectCompletDetailsModel.getAddress() != null && !projectCompletDetailsModel.getAddress().isEmpty()) {
            address.setVisibility(View.VISIBLE);
            address.setText("Address: " + checkNull(projectCompletDetailsModel.getAddress()));
        } else {
            address.setVisibility(View.GONE);
        }

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        newLeadsFragment.getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);


    }


    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public String checkNullNA(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "NA";
    }

    @Override
    public int getItemCount() {
        return newLeadsResponseModels.size();
    }

    public void updateRefernceStatus() {

    }


    public void getAvailableCareTakers(final String project_id, final Spinner spinner, final EditText quotedPrice, final Button confirmQuote, final Button quote) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<AvailableCareTakersResponseModel> call = apiService.getAvailableCareTakersofOrganisation(Prefs.getString("organisationID", ""));
        call.enqueue(new Callback<AvailableCareTakersResponseModel>() {
            @Override
            public void onResponse(Call<AvailableCareTakersResponseModel> call, final Response<AvailableCareTakersResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        final String[] selectedCareTakerID = {""};
                        customProgressDialog.cancel();
                        final Resources[] res = {context.getResources()};

                        // Create custom adapter object ( see below CustomAdapter.java )
                        CareTakersSpinnerAdapter adapter = new CareTakersSpinnerAdapter(context, R.layout.list_item_availablecaretaker, (ArrayList) response.body().getTasks(), res[0]);

                        // Set adapter to spinner
                        spinner.setAdapter(adapter);
                        spinner.setVisibility(View.VISIBLE);
                        quotedPrice.setVisibility(View.VISIBLE);
                        confirmQuote.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);

                        confirmQuote.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                confirmQuote(project_id, selectedCareTakerID[0], quotedPrice.getText().toString(), spinner, quotedPrice, confirmQuote, quote);

                            }
                        });


                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                                // your code here

                                // Get selected row data to show on screen
                                selectedCareTakerID[0] = response.body().getTasks().get(position).getId();


                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }

                        });


                    } else {
                        customProgressDialog.cancel();
                        Toast.makeText(context, "cartakers not available", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    customProgressDialog.cancel();

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<AvailableCareTakersResponseModel> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();
                Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show();

            }


        });
    }

    public void confirmQuote(String project_id, String cartaker_id, final String coated_price, final Spinner cartakers, final EditText coatedPrice, final Button confirmQuote, final Button quotes) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.confimQuote(project_id, cartaker_id, coated_price);

        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {


                ;
                if (response != null && response.body() != null && response.body().getError() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    newLeadsFragment.getOngoingProjectsData();
                    confirmQuote.setVisibility(View.GONE);
                    coatedPrice.setVisibility(View.GONE);
                    cartakers.setVisibility(View.GONE);
                    quotes.setVisibility(View.VISIBLE);
                    customProgressDialog.cancel();
                } else {
                    if (response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        customProgressDialog.cancel();

                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                        customProgressDialog.cancel();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                customProgressDialog.cancel();
                ;
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
