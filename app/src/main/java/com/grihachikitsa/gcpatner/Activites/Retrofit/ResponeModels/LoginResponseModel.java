package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

/**
 * Created by Nikil on 12/25/2016.
 */
public class LoginResponseModel {



    private String id;
    private String name;
    private String concerened_person;
    private String email;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String error;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConcerened_person() {
        return concerened_person;
    }

    public void setConcerened_person(String concerened_person) {
        this.concerened_person = concerened_person;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    private String password;
    private String mobile;
    private String message;
    private String status;
    private String created_at;
}
