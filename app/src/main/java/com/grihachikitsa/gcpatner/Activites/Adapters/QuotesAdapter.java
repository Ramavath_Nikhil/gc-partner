package com.grihachikitsa.gcpatner.Activites.Adapters;

/**
 * Created by Nikil on 12/8/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.grihachikitsa.gcpatner.Activites.Activites.QuotedProjectsActivity;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.AvailableCareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProjectCompletDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.QuotesModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.MyViewHolder> {

    private List<QuotesModel> ongoingProjectsModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name, age, weight, gender, service, timePeriod, address, startDate, duration, caretakerName, coatedPrice;
        public Button update, cancel, confirmQuote;
        public Spinner careTaker;
        public EditText quotedPrice;
        public LinearLayout mainlayout, bottomLayout;

        public MyViewHolder(View view) {
            super(view);
            //careType = (TextView) view.findViewById(R.id.careType);
            name = (TextView) view.findViewById(R.id.name);
            age = (TextView) view.findViewById(R.id.age);
            weight = (TextView) view.findViewById(R.id.weight);
            service = (TextView) view.findViewById(R.id.serivces);
            timePeriod = (TextView) view.findViewById(R.id.time);
            address = (TextView) view.findViewById(R.id.address);
            startDate = (TextView) view.findViewById(R.id.startdate);
            duration = (TextView) view.findViewById(R.id.end_day);
            gender = (TextView) view.findViewById(R.id.gender);
            update = (Button) view.findViewById(R.id.update);
            caretakerName = (TextView) view.findViewById(R.id.caretakerName);
            coatedPrice = (TextView) view.findViewById(R.id.coatedPrice);
            mainlayout = (LinearLayout) view.findViewById(R.id.mainlayout);
            cancel = (Button) view.findViewById(R.id.cancel);
            careTaker = (Spinner) view.findViewById(R.id.careTaker);
            confirmQuote = (Button) view.findViewById(R.id.confirmQuote);
            quotedPrice = (EditText) view.findViewById(R.id.quotedPrice);
            bottomLayout = (LinearLayout) view.findViewById(R.id.bottomlayout);
        }
    }


    public QuotesAdapter(List<QuotesModel> moviesList, Context context) {
        this.ongoingProjectsModelList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_quotedprojects, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final QuotesModel ongoingProjectsModel = ongoingProjectsModelList.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());


        holder.name.setText(checkNull(ongoingProjectsModel.getPatient_name()));
        holder.address.setText("Address: " + checkNullNA(ongoingProjectsModel.getAddress()));
        holder.timePeriod.setText("Time Period: " + checkNullNA(ongoingProjectsModel.getHours()));
        holder.service.setText("Services: " + checkNullNA(ongoingProjectsModel.getServices()));
        holder.age.setText("Age: " + checkNullNA(ongoingProjectsModel.getAge()));
        holder.weight.setText("Weight: " + checkNullNA(ongoingProjectsModel.getWeight()));
        holder.gender.setText("Gender: " + checkNullNA(ongoingProjectsModel.getGender()));

        holder.duration.setText("Duration: " + checkNullNA(ongoingProjectsModel.getDuration()));
        holder.caretakerName.setText("Caretaker: " + checkNullNA(ongoingProjectsModel.getCaretaker_name()));
        if (Integer.parseInt(ongoingProjectsModel.getProjectquotes_coatedprice()) > 0) {
            holder.coatedPrice.setText("Coated price: Rs" + checkNull(ongoingProjectsModel.getProjectquotes_coatedprice()));
            holder.coatedPrice.setTextColor(holder.gender.getCurrentTextColor());
            holder.startDate.setText("Quoted Date: " + checkNullNA(converDateTime(ongoingProjectsModel.getProject_coated_on())));
        } else {
            holder.coatedPrice.setText("Status: Cancelled");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.coatedPrice.setTextColor(context.getColor(android.R.color.holo_red_dark));
            } else {
                holder.coatedPrice.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
            }
            holder.startDate.setText("Cancelled on: " + checkNullNA(converDateTime(ongoingProjectsModel.getProject_coated_on())));
        }
        holder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.bottomLayout.setVisibility(View.GONE);
             /*   holder.careTaker.setVisibility(View.VISIBLE);
                holder.quotedPrice.setVisibility(View.VISIBLE);
                holder.confirmQuote.setVisibility(View.VISIBLE);
*/
                getAvailableCareTakers(ongoingProjectsModel.getProjectquote_id(), holder.careTaker, holder.quotedPrice, holder.confirmQuote, holder.update, ongoingProjectsModel.getCaretaker_name(), ongoingProjectsModel.getProject_caretaker_id(), position);
            }
        });

        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cancelQuote(ongoingProjectsModel.getProject_caretaker_id(), ongoingProjectsModel.getProjectquote_id(), position);
            }
        });
        holder.mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProjectDetails(ongoingProjectsModel.getId());
            }
        });

    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    public void getAvailableCareTakers(final String quote_id, final Spinner spinner, final EditText quotedPrice, final Button confirmQuote, final Button quote, final String caretakerName, final String cartakerID, final int position) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
        final ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<AvailableCareTakersResponseModel> call = apiService.getAvailableCareTakersofOrganisation(Prefs.getString("organisationID", ""));
        call.enqueue(new Callback<AvailableCareTakersResponseModel>() {
            @Override
            public void onResponse(Call<AvailableCareTakersResponseModel> call, final Response<AvailableCareTakersResponseModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        final String[] selectedCareTakerID = {"", ""};
                        customProgressDialog.cancel();
                        final Resources[] res = {context.getResources()};


                        if (Integer.parseInt(ongoingProjectsModelList.get(position).getProjectquotes_coatedprice()) > 0) {
                            AvailableCareTakersResponseModel availableCareTakersResponseModel = new AvailableCareTakersResponseModel();
                            availableCareTakersResponseModel.setId(cartakerID);
                            availableCareTakersResponseModel.setCaretaker_name(caretakerName);
                            response.body().getTasks().add(0, availableCareTakersResponseModel);
                        }

                        // Create custom adapter object ( see below CustomAdapter.java )
                        CareTakersSpinnerAdapter adapter = new CareTakersSpinnerAdapter(context, R.layout.list_item_availablecaretaker, (ArrayList) response.body().getTasks(), res[0]);

                        // Set adapter to spinner
                        spinner.setAdapter(adapter);
                        spinner.setVisibility(View.VISIBLE);
                        quotedPrice.setVisibility(View.VISIBLE);
                        confirmQuote.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);

                        confirmQuote.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                updateQuoteDetails(selectedCareTakerID[0], quotedPrice.getText().toString(), quote_id, cartakerID, selectedCareTakerID[1], position);
                            }
                        });


                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                                // your code here

                                // Get selected row data to show on screen
                                selectedCareTakerID[0] = response.body().getTasks().get(position).getId();
                                selectedCareTakerID[1] = response.body().getTasks().get(position).getCaretaker_name();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }

                        });


                    } else {
                        customProgressDialog.cancel();
                        // Toast.makeText(context, "cartakers not available", Toast.LENGTH_SHORT).show();

                        final String[] selectedCareTakerID = {"", ""};
                        customProgressDialog.cancel();
                        final Resources[] res = {context.getResources()};

                        List<AvailableCareTakersResponseModel> availableCareTakersResponseModelList = new ArrayList<AvailableCareTakersResponseModel>();
                        if (Integer.parseInt(ongoingProjectsModelList.get(position).getProjectquotes_coatedprice()) > 0) {
                            AvailableCareTakersResponseModel availableCareTakersResponseModel = new AvailableCareTakersResponseModel();
                            availableCareTakersResponseModel.setId(cartakerID);
                            availableCareTakersResponseModel.setCaretaker_name(caretakerName);
                            response.body().getTasks().add(0, availableCareTakersResponseModel);
                            availableCareTakersResponseModelList.add(availableCareTakersResponseModel);
                        }


                        // Create custom adapter object ( see below CustomAdapter.java )
                        CareTakersSpinnerAdapter adapter = new CareTakersSpinnerAdapter(context, R.layout.list_item_availablecaretaker, (ArrayList) availableCareTakersResponseModelList, res[0]);

                        // Set adapter to spinner
                        spinner.setAdapter(adapter);
                        spinner.setVisibility(View.VISIBLE);
                        quotedPrice.setVisibility(View.VISIBLE);
                        confirmQuote.setVisibility(View.VISIBLE);
                        quote.setVisibility(View.GONE);

                        confirmQuote.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                updateQuoteDetails(selectedCareTakerID[0], quotedPrice.getText().toString(), quote_id, cartakerID, selectedCareTakerID[1], position);
                            }
                        });


                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parentView, View v, int position, long id) {
                                // your code here

                                // Get selected row data to show on screen
                                selectedCareTakerID[0] = response.body().getTasks().get(position).getId();
                                selectedCareTakerID[1] = response.body().getTasks().get(position).getCaretaker_name();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parentView) {
                                // your code here
                            }

                        });


                    }

                } else {
                    customProgressDialog.cancel();

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<AvailableCareTakersResponseModel> call, Throwable t) {

                t.printStackTrace();
                customProgressDialog.cancel();
                Toast.makeText(context, "something went wrong", Toast.LENGTH_SHORT).show();

            }


        });
    }

    public void updateQuoteDetails(final String caretakerId, final String coatedPrice, String quote_id, String oldCaretakerID, final String careTakerName, final int position) {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.updateQuoteDetails(caretakerId, coatedPrice, quote_id, oldCaretakerID);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                customProgressDialog.cancel();
                ;
                if (response.body() != null && response.body().getError() != null && response.body().getError().equalsIgnoreCase("false")) {
                    Toast.makeText(context, "Updated details", Toast.LENGTH_SHORT).show();


                    ongoingProjectsModelList.get(position).setProject_caretaker_id(caretakerId);
                    ongoingProjectsModelList.get(position).setProjectquotes_coatedprice(coatedPrice);
                    ongoingProjectsModelList.get(position).setCaretaker_name(careTakerName);
                    notifyDataSetChanged();
                } else {
                    if (response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                ;
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void cancelQuote(String quote_id, String caretaker_id, final int position) {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.cancelQuote(caretaker_id, quote_id);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                if (response.body().getError() != null && response.body().getError().equalsIgnoreCase("false")) {
                    customProgressDialog.cancel();
                    Toast.makeText(context, "Updated details", Toast.LENGTH_SHORT).show();
                    ongoingProjectsModelList.get(position).setProjectquotes_coatedprice("-1");
                    Calendar calendar = Calendar.getInstance();
                    ongoingProjectsModelList.get(position).setProject_coated_on(calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DATE) + " " + calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
                    notifyDataSetChanged();
                } else {
                    customProgressDialog.cancel();
                    if (response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getProjectDetails(String project_id) {


        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProjectCompletDetailsModel> call = apiService.getCompleteDetailsOfProject(project_id);
        call.enqueue(new Callback<ProjectCompletDetailsModel>() {
            @Override
            public void onResponse(Call<ProjectCompletDetailsModel> call, Response<ProjectCompletDetailsModel> response) {
                customProgressDialog.cancel();
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false") && response.body().getTasks() != null && response.body().getTasks().size() > 0 && response.body().getTasks().get(0) != null) {
                    ProjectCompletDetailsModel projectCompletDetailsModel = response.body().getTasks().get(0);

                    printProjectDetails(projectCompletDetailsModel);

                } else {

                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProjectCompletDetailsModel> call, Throwable t) {

                customProgressDialog.cancel();

                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void printProjectDetails(ProjectCompletDetailsModel projectCompletDetailsModel) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_complete_details_of_project);


        // set the custom dialog components - text, image and button


        TextView patientName, patientAge, patientHeight, patientWeight, patientGender, patientServices, hours, duration, moreinfo, address;
        TextView quotes, careTakerName, organisationName, coatedPrice, acceptedOn;
        Button viewQuotes, careTakerDetails, organisationDetails;
        LinearLayout careTakerDetailsLayout;
        TextView careTakerDetailsTitle;


        //TEXTVIEW INTILIZATION
        patientName = (TextView) dialog.findViewById(R.id.name);
        patientAge = (TextView) dialog.findViewById(R.id.age);
        patientHeight = (TextView) dialog.findViewById(R.id.height);
        patientWeight = (TextView) dialog.findViewById(R.id.weight);
        patientGender = (TextView) dialog.findViewById(R.id.gender);
        patientServices = (TextView) dialog.findViewById(R.id.serivces);
        hours = (TextView) dialog.findViewById(R.id.hours);
        duration = (TextView) dialog.findViewById(R.id.duration);
        moreinfo = (TextView) dialog.findViewById(R.id.moreinfo);
        address = (TextView) dialog.findViewById(R.id.address);
        quotes = (TextView) dialog.findViewById(R.id.quotes);
        careTakerName = (TextView) dialog.findViewById(R.id.careTakerName);
        organisationName = (TextView) dialog.findViewById(R.id.organisationName);
        coatedPrice = (TextView) dialog.findViewById(R.id.coatedPrice);
        acceptedOn = (TextView) dialog.findViewById(R.id.coatedOn);
        careTakerDetailsTitle = (TextView) dialog.findViewById(R.id.careTakerDetails);

        //BUTTON INTILIZATION
        viewQuotes = (Button) dialog.findViewById(R.id.viewQuotes);
        careTakerDetails = (Button) dialog.findViewById(R.id.careTakerCompleteDetails);
        organisationDetails = (Button) dialog.findViewById(R.id.organisationCompleteDetails);

        //LINEAR LAYOUT INTILIZATION
        careTakerDetailsLayout = (LinearLayout) dialog.findViewById(R.id.careTakerDetailsLayout);


        patientName.setText("Patient Name: " + checkNull(projectCompletDetailsModel.getPatient_name()));
        patientAge.setText("Age: " + checkNull(projectCompletDetailsModel.getAge()));
        patientGender.setText("Gender: " + checkNull(projectCompletDetailsModel.getGender()));
        patientHeight.setText("Height: " + checkNull(projectCompletDetailsModel.getHeight()));
        patientWeight.setText("Weight: " + checkNull(projectCompletDetailsModel.getWeight()));
        patientServices.setText("Services: " + checkNull(projectCompletDetailsModel.getServices()));
        hours.setText("Time Period: " + checkNull(projectCompletDetailsModel.getHours()));
        duration.setText("Duration: " + checkNull(projectCompletDetailsModel.getDuration()));
        if (projectCompletDetailsModel.getMoreinfo() != null && !projectCompletDetailsModel.getMoreinfo().isEmpty()) {
            moreinfo.setVisibility(View.VISIBLE);
            moreinfo.setText("More info: " + checkNull(projectCompletDetailsModel.getMoreinfo()));
        } else {
            moreinfo.setVisibility(View.GONE);
        }

        if (projectCompletDetailsModel.getAddress() != null && !projectCompletDetailsModel.getAddress().isEmpty()) {
            address.setVisibility(View.VISIBLE);
            address.setText("Address: " + checkNull(projectCompletDetailsModel.getAddress()));
        } else {
            address.setVisibility(View.GONE);
        }

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((QuotedProjectsActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);


    }

    public void completeProject(String project_id, final int position) {


        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.updateProjectCompletion(project_id);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                customProgressDialog.cancel();
                if (response.body() != null && response.body().getError() != null && response.body().getError().equalsIgnoreCase("false")) {
                    if (response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        ongoingProjectsModelList.remove(position);
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Project completed", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public String checkNullNA(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "NA";
    }

    @Override
    public int getItemCount() {
        return ongoingProjectsModelList.size();
    }

    public void updateRefernceStatus() {

    }

}
