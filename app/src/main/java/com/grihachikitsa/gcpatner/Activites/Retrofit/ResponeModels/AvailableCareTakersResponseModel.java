package com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Nikil on 12/30/2016.
 */
public class AvailableCareTakersResponseModel implements Serializable {



    private String caretaker_name,id,error;
    private List<AvailableCareTakersResponseModel> tasks;

    public String getCaretaker_name() {
        return caretaker_name;
    }

    public void setCaretaker_name(String caretaker_name) {
        this.caretaker_name = caretaker_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<AvailableCareTakersResponseModel> getTasks() {
        return tasks;
    }

    public void setTasks(List<AvailableCareTakersResponseModel> tasks) {
        this.tasks = tasks;
    }
}
