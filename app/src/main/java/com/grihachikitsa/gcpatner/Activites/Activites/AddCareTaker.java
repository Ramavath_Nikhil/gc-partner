package com.grihachikitsa.gcpatner.Activites.Activites;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetFragment_ChooseImage;
import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetLanguageSelectionFragment;
import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetSelectionFragment;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCareTaker extends AppCompatActivity {


    private EditText first_name, last_name, email, mobile, gender, age, doj, qualification, experience, designation, address, residenceNumber, id_addressProof, bloodGroup, father, mother, spouse, martialStatus, childre, backgroundVerification;
    private EditText height, weight, language, policeCertificate, extraCertificate;
    private Button addCareTaker;
    private Uri fileUri;
    public static int MEDIA_TYPE_IMAGE = 22;
    public static String IMAGE_DIRECTORY_NAME = "grihachikitsa";
    private int PICK_IMAGES = 1;
    final int SELECT_PICTURE = 2, TAKE_PICTURE = 3, TAKE_AADHAR = 4, TAKE_POLICE_CERTIFITCATE = 5, TAKE_EXTRA_CERTIFICATE = 6, SELECT_AADHAR = 7, SELECT_POLICE_CERTIFICATE = 8, SELECT_EXTRA_CERTIFICATE = 9;
    private Bitmap thumbnail_r;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private String imagePath = "", aadharPath = "", policeCertifitcatePath = "", extraCertificatePath = "";
    private CircleImageView profilePic;
    private FloatingActionButton profilePicEdit;
    private RelativeLayout mainlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_care_taker);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }

        elementsIntilization();
        onClickListener();
    }

    public void elementsIntilization() {
        //EDITTEXT INTILIZATION
        first_name = (EditText) findViewById(R.id.firstname);
        last_name = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        gender = (EditText) findViewById(R.id.gender);
        age = (EditText) findViewById(R.id.age);
        doj = (EditText) findViewById(R.id.doj);
        qualification = (EditText) findViewById(R.id.qualification);
        experience = (EditText) findViewById(R.id.experience);
        designation = (EditText) findViewById(R.id.designation);
        residenceNumber = (EditText) findViewById(R.id.residncenumber);
        id_addressProof = (EditText) findViewById(R.id.addressProof);
        bloodGroup = (EditText) findViewById(R.id.bloodgroup);
        father = (EditText) findViewById(R.id.father);
        mother = (EditText) findViewById(R.id.mother);
        martialStatus = (EditText) findViewById(R.id.martial_status);
        spouse = (EditText) findViewById(R.id.spouse);
        childre = (EditText) findViewById(R.id.children);
        backgroundVerification = (EditText) findViewById(R.id.backgroundVerification);
        address = (EditText) findViewById(R.id.address);
        height = (EditText) findViewById(R.id.height);
        weight = (EditText) findViewById(R.id.weight);
        language = (EditText) findViewById(R.id.language);
        policeCertificate = (EditText) findViewById(R.id.policeCertificateProof);
        extraCertificate = (EditText) findViewById(R.id.extraCertificateProof);

        //BUTTON ON CLICK LISTENER
        addCareTaker = (Button) findViewById(R.id.addCareTaker);

        //CIRCULAT IMAGE VIEW
        profilePic = (CircleImageView) findViewById(R.id.profilepic);

        //FLOATING BUTTON INTILIZATION
        profilePicEdit = (FloatingActionButton) findViewById(R.id.edit);

        //RELATIVE LAYOUT INTILIZATION
        mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
    }

    public void onClickListener() {
        addCareTaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkRequiredFields();
            }
        });

        //EDITEXT ONCLICK LISTENERS
        doj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker();
            }
        });

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> gender = new ArrayList<String>();
                gender.add("Male");
                gender.add("Female");
                gender.add("Other");
                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(gender, "gender");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });

        martialStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> martialStatus = new ArrayList<String>();
                martialStatus.add("Single");
                martialStatus.add("Married");
                martialStatus.add("Divorced");
                martialStatus.add("Widow");
                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(martialStatus, "martialStatus");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });

        backgroundVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> backgroundVerification = new ArrayList<String>();
                backgroundVerification.add("Verified");
                backgroundVerification.add("Not Verified");

                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(backgroundVerification, "backgroundVerification");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });
        bloodGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                List<String> bloodGroup = new ArrayList<String>();
                bloodGroup.add("A +");
                bloodGroup.add("A -");
                bloodGroup.add("B +");
                bloodGroup.add("B -");
                bloodGroup.add("O +");
                bloodGroup.add("O -");
                bloodGroup.add("AB +");
                bloodGroup.add("AB -");

                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(bloodGroup, "bloodGroup");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });

        language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(getLanguages(), "language", language.getText().toString().split(","));
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });

        height.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> height = new ArrayList<String>();
                height.add("4.5 - 5.0 Feet");
                height.add("5.0 - 5.5 Feet");
                height.add("5.5 - 6.0 Feet");
                height.add("ABOVE 6 Feet");


                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(height, "height");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });

        weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> weight = new ArrayList<String>();
                weight.add("BELOW 55 kgs");
                weight.add("55-64 kgs");
                weight.add("65-74 kgs");
                weight.add("75-84 kgs");
                weight.add("ABOVE 85 kgs");


                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(weight, "weight");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });

        designation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> weight = new ArrayList<String>();
                weight.add("Nurse");
                weight.add("Care Pro");



                final BottomSheetSelectionFragment myBottomSheet = BottomSheetSelectionFragment.newInstance(weight, "designation");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });


        //FLOATING ACTTION BUTTON ONCLICK LISTENER
        profilePicEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(AddCareTaker.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    Log.d("inside permission", "true");
                    ActivityCompat.requestPermissions(AddCareTaker.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("Modal Bottom Sheet");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }

            }
        });

        id_addressProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(AddCareTaker.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    Log.d("inside permission", "true");
                    ActivityCompat.requestPermissions(AddCareTaker.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("aadhar");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }
            }
        });

        policeCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(AddCareTaker.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    Log.d("inside permission", "true");
                    ActivityCompat.requestPermissions(AddCareTaker.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("policecertificate");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }
            }
        });

        extraCertificate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(AddCareTaker.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    Log.d("inside permission", "true");
                    ActivityCompat.requestPermissions(AddCareTaker.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("extracertificate");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }
            }
        });

        //CIRCULAT IMAGEVIEW ONCLICK LISTENER
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(AddCareTaker.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    ActivityCompat.requestPermissions(AddCareTaker.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImage myBottomSheet = BottomSheetFragment_ChooseImage.newInstance("Modal Bottom Sheet");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }

            }
        });

    }

    public List<String> getLanguages() {
        List<String> languages = new ArrayList<>();
        languages.add("Hindi");
        languages.add("English");
        languages.add("Bengali");
        languages.add("Telugu");
        languages.add("Marathi");
        languages.add("Tamil");
        languages.add("Urdu");
        languages.add("Kannada");
        languages.add("Gujarati");
        languages.add("Odia");
        languages.add("Malayalam");
        languages.add("Sanskrit");
        return languages;
    }

    public void checkRequiredFields() {
        if (!first_name.getText().toString().isEmpty()) {
            if (!last_name.getText().toString().isEmpty()) {
                if (!last_name.getText().toString().isEmpty()) {
                    if (!gender.getText().toString().isEmpty()) {
                        if (!age.getText().toString().isEmpty()) {
                            if (!age.getText().toString().isEmpty()) {
                                if (!age.getText().toString().isEmpty()) {
                                    if (!experience.getText().toString().isEmpty()) {
                                        if (!designation.getText().toString().isEmpty()) {
                                            if (!id_addressProof.getText().toString().isEmpty()) {
                                                if (!bloodGroup.getText().toString().isEmpty()) {
                                                    if (!martialStatus.getText().toString().isEmpty()) {
                                                        if (!martialStatus.getText().toString().isEmpty()) {

                                                            if (!address.getText().toString().isEmpty()) {

                                                                if (!height.getText().toString().isEmpty()) {
                                                                    if (!weight.getText().toString().isEmpty()) {
                                                                        if (!language.getText().toString().isEmpty()) {
                                                                            if (imagePath != null && !imagePath.isEmpty()) {

                                                                                if (aadharPath != null && !aadharPath.isEmpty()) {
                                                                                    if (policeCertifitcatePath != null && !policeCertifitcatePath.isEmpty()) {

                                                                                        if (backgroundVerification.getText().toString().equalsIgnoreCase("verified")) {
                                                                                            uploadImage(imagePath, "1", aadharPath, policeCertifitcatePath, extraCertificatePath);
                                                                                        } else {
                                                                                            uploadImage(imagePath, "0", aadharPath, policeCertifitcatePath, extraCertificatePath);
                                                                                        }

                                                                                    } else {
                                                                                        Toast.makeText(AddCareTaker.this, "Please upload police verification certificate", Toast.LENGTH_SHORT).show();
                                                                                    }
                                                                                } else {

                                                                                    Toast.makeText(AddCareTaker.this, "Please upload aadhar card", Toast.LENGTH_SHORT).show();

                                                                                }

                                                                            } else {
                                                                                Toast.makeText(AddCareTaker.this, "please select profile picture", Toast.LENGTH_SHORT).show();
                                                                            }


                                                                        } else {
                                                                            Toast.makeText(AddCareTaker.this, "Please provide known languages", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    } else {

                                                                        Toast.makeText(AddCareTaker.this, "Please provide weight", Toast.LENGTH_SHORT).show();

                                                                    }
                                                                } else {
                                                                    Toast.makeText(AddCareTaker.this, "Please provide height", Toast.LENGTH_SHORT).show();
                                                                }


                                                            } else {
                                                                Toast.makeText(AddCareTaker.this, "Please provide address details", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            Toast.makeText(AddCareTaker.this, "Please provide martial status details", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Toast.makeText(AddCareTaker.this, "Please provide martial status details", Toast.LENGTH_SHORT).show();

                                                    }
                                                } else {
                                                    Toast.makeText(AddCareTaker.this, "Please provide blood group details", Toast.LENGTH_SHORT).show();

                                                }
                                            } else {
                                                Toast.makeText(AddCareTaker.this, "Please provide ID/Address proof details", Toast.LENGTH_SHORT).show();

                                            }
                                        } else {
                                            Toast.makeText(AddCareTaker.this, "Please provide designation details", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {
                                        Toast.makeText(AddCareTaker.this, "Please provide experience details", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(AddCareTaker.this, "Please provide age details", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(AddCareTaker.this, "Please provide age details", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(AddCareTaker.this, "Please provide age details", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(AddCareTaker.this, "Please provide gender details", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(AddCareTaker.this, "Please provide last name", Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(AddCareTaker.this, "Please provide last name", Toast.LENGTH_SHORT).show();

            }
        } else {
            Toast.makeText(AddCareTaker.this, "Please provide first name", Toast.LENGTH_SHORT).show();

        }


    }

    public void datePicker() {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        doj.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void setText(String type, String selectedText) {
        if (type.equalsIgnoreCase("gender")) {
            gender.setText(selectedText);

        } else if (type.equalsIgnoreCase("backgroundVerification")) {
            backgroundVerification.setText(selectedText);

        } else if (type.equalsIgnoreCase("bloodGroup")) {
            bloodGroup.setText(selectedText);

        } else if (type.equalsIgnoreCase("martialStatus")) {
            if (selectedText.equalsIgnoreCase("single")) {
                spouse.setVisibility(View.GONE);
                childre.setVisibility(View.GONE);
            } else {
                spouse.setVisibility(View.VISIBLE);
                childre.setVisibility(View.VISIBLE);
            }

            martialStatus.setText(selectedText);
        } else if (type.equalsIgnoreCase("height")) {
            height.setText(selectedText);
        } else if (type.equalsIgnoreCase("weight")) {
            weight.setText(selectedText);
        } else if (type.equalsIgnoreCase("language")) {
            language.setText(selectedText);
        }
        else if (type.equalsIgnoreCase("designation"))
        {
            designation.setText(selectedText);
        }
    }

    public void createCareTaker(String backgroundVerification) {
       /* final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(AddCareTaker.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.createCareTaker(first_name.getText().toString() + " " + last_name.getText().toString(), Prefs.getString("organisationID", ""), email.getText().toString(), mobile.getText().toString(), gender.getText().toString(), age.getText().toString(), doj.getText().toString()
                , qualification.getText().toString(), experience.getText().toString(), designation.getText().toString(), id_addressProof.getText().toString(), bloodGroup.getText().toString(), martialStatus.getText().toString(), backgroundVerification
                , address.getText().toString(), residenceNumber.getText().toString(), father.getText().toString(), mother.getText().toString(), spouse.getText().toString(), childre.getText().toString());
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                if (response != null && response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    customProgressDialog.cancel();
                    Toast.makeText(AddCareTaker.this, "Caretaker registered succesfully", Toast.LENGTH_SHORT).show();
                } else {
                    customProgressDialog.cancel();
                    if (response != null && response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(AddCareTaker.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddCareTaker.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(AddCareTaker.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void uploadImage(final String imagepath, String backgroundVerification, final String aadharPath, String policeCertifitcatePath, String extraCertificatePath) {

        /**
         * Progressbar to Display if you need
         */
        final CustomProgressDialog progressDialog = CustomProgressDialog.show(AddCareTaker.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagepath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);

        MultipartBody.Part aadharcard =
                MultipartBody.Part.createFormData("aadharcard", file.getName(), requestFile);

        MultipartBody.Part policeVerificationCertificate =
                MultipartBody.Part.createFormData("policeverificationcertificate", file.getName(), requestFile);

        MultipartBody.Part extraCertificate =
                MultipartBody.Part.createFormData("extracertificate", file.getName(), requestFile);
        final RequestBody requestEmail =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), email.getText().toString());
        RequestBody requestOrganisationID =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), Prefs.getString("organisationID", ""));
        RequestBody requestMobile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), mobile.getText().toString());
        RequestBody requestFirstName =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), first_name.getText().toString() + " " + last_name.getText().toString());

        RequestBody requestGender =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), gender.getText().toString());
        RequestBody requestAge =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), age.getText().toString());

        RequestBody requestDoj =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), doj.getText().toString());
        RequestBody requestQualification =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), qualification.getText().toString());

        RequestBody requestExperience =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), experience.getText().toString());

        RequestBody requestDesignation =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), designation.getText().toString());
        RequestBody requestAddressProof =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), id_addressProof.getText().toString());

        RequestBody requestBloodGroup =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), bloodGroup.getText().toString());

        RequestBody requestMartialStatus =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), martialStatus.getText().toString());

        RequestBody requestHeight =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), height.getText().toString());

        RequestBody requestWeight =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), weight.getText().toString());

        RequestBody requestLanguages =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), language.getText().toString());


        RequestBody requestBackgroundVerification =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), backgroundVerification);
        RequestBody requestAddress =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), address.getText().toString());
        RequestBody requestResidenceNumber =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), residenceNumber.getText().toString());
        RequestBody requestFather =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), father.getText().toString());
        RequestBody requestMother =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), mother.getText().toString());

        RequestBody requestSpouse =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), spouse.getText().toString());
        RequestBody requestChildren =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), childre.getText().toString());


        Call<RegisterResponseModel> resultCall = apiService.createCareTaker(body, requestFirstName, requestOrganisationID, requestEmail, requestMobile, requestGender, requestAge, requestDoj, requestQualification, requestExperience, requestDesignation, requestAddressProof, requestBloodGroup, requestMartialStatus, requestBackgroundVerification, requestAddress, requestResidenceNumber, requestFather, requestMother, requestSpouse, requestChildren, requestHeight, requestWeight, requestLanguages);

        resultCall.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                progressDialog.cancel();
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    clearForm(mainlayout);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        profilePic.setImageDrawable(getDrawable(R.drawable.ic_person));
                    } else {
                        profilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_person));
                    }
                    imagePath = "";

                    if (response.body().getId() != null && !response.body().getId().isEmpty()) {

                        Toast.makeText(AddCareTaker.this,"Uploading Aadhar card",Toast.LENGTH_SHORT).show();
                        updateAaharCard(aadharPath, response.body().getId(), response.body().getMessage());
                    } else {
                        Toast.makeText(AddCareTaker.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }


                } else {

                    if (response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(AddCareTaker.this, response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(AddCareTaker.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                progressDialog.cancel();
                Toast.makeText(AddCareTaker.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void updateAaharCard(String imagePath, final String user_id, final String message) {

        final CustomProgressDialog progressDialog = CustomProgressDialog.show(AddCareTaker.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        RequestBody requestID =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), user_id);


        Call<RegisterResponseModel> resultCall = apiService.updateAadharCard(body, requestID);
        resultCall.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.cancel();

                aadharPath="";
                if (response!=null && response.body()!=null && response.body().getError().equalsIgnoreCase("false")) {

                    updatePoliceCertificate(policeCertifitcatePath, user_id, message);
                    Toast.makeText(AddCareTaker.this, "Updating police certificate", Toast.LENGTH_SHORT).show();
                } else {
                   // Toast.makeText(AddCareTaker.this, "Message: " + message, Toast.LENGTH_SHORT).show();
                    updatePoliceCertificate(policeCertifitcatePath, user_id, message);
                    Toast.makeText(AddCareTaker.this, "Updating police certificate", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                aadharPath="";
                policeCertifitcatePath="";
                extraCertificatePath="";
                progressDialog.cancel();
                Toast.makeText(AddCareTaker.this, "Message: " + message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clearForm(ViewGroup group) {
        for (int i = 0, count = group.getChildCount(); i < count; ++i) {
            View view = group.getChildAt(i);
            if (view instanceof EditText) {
                ((EditText) view).setText("");
            }

            if (view instanceof ViewGroup && (((ViewGroup) view).getChildCount() > 0))
                clearForm((ViewGroup) view);
        }
    }

    public String checkNull(String s)

    {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public void takePicture() {

        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_PICTURE);
    }

    public void updatePoliceCertificate(String imagePath, final String user_id, final String message) {


        Log.d("police path",imagePath+"");
        final CustomProgressDialog progressDialog = CustomProgressDialog.show(AddCareTaker.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        RequestBody requestID =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), user_id);


        Call<RegisterResponseModel> resultCall = apiService.updatePoliceCertifcateOfCaretaker(body, requestID);
        resultCall.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.cancel();
                policeCertifitcatePath="";
                if (response!=null && response.body()!=null &&response.body().getError().equalsIgnoreCase("false")) {
                    if (extraCertificatePath != null && !extraCertificatePath.isEmpty()) {
                        updateExtraCertificate(extraCertificatePath, user_id, message);
                        Toast.makeText(AddCareTaker.this, "Updating extra certificate", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(AddCareTaker.this, "Message: " + message, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                policeCertifitcatePath="";
                extraCertificatePath="";
                progressDialog.cancel();
                Toast.makeText(AddCareTaker.this, "Message: " + message, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void updateExtraCertificate(String imagePath, String user_id, final String message) {

        Log.d("extra path",imagePath+"");
        final CustomProgressDialog progressDialog = CustomProgressDialog.show(AddCareTaker.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        RequestBody requestID =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"), user_id);


        Call<RegisterResponseModel> resultCall = apiService.updateExtraCertificate(body, requestID);
        resultCall.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.cancel();
                extraCertificatePath="";

                Toast.makeText(AddCareTaker.this, "Message: " + message, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                progressDialog.cancel();
                extraCertificatePath="";
                Toast.makeText(AddCareTaker.this, "Message: " + message, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void takeAadharPicture() {
        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_AADHAR);
    }

    public void takePoliceCertificate() {
        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_POLICE_CERTIFITCATE);
    }

    public void takeExtraCertificate() {
        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_EXTRA_CERTIFICATE);
    }


    public void selectgImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_PICTURE);

    }

    public void selectgAadharFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_AADHAR);

    }

    public void selectgPoliceCertificateFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_POLICE_CERTIFICATE);

    }

    public void selectgExtraCertificateFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_EXTRA_CERTIFICATE);

    }


    // for roted image......
    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } catch (OutOfMemoryError err) {
            source.recycle();
            Date d = new Date();
            CharSequence s = DateFormat
                    .format("MM-dd-yy-hh-mm-ss", d.getTime());
            String fullPath = Environment.getExternalStorageDirectory()
                    + "/RYB_pic/" + s.toString() + ".jpg";
            if ((fullPath != null) && (new File(fullPath).exists())) {
                new File(fullPath).delete();
            }
            bitmap = null;
            err.printStackTrace();
        }
        return bitmap;
    }


    public static Bitmap decodeSampledBitmapFromResource(String pathToFile,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        Log.e("inSampleSize", "inSampleSize______________in storage"
                + options.inSampleSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathToFile, options);
    }


    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

        }

        return inSampleSize;
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    public void startImageSelection() {
        Intent intent = new Intent();

        intent.setType("image/*");


        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap thePic = null;
                try {
                    thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();

                  /*  FaceCrop faceCrop = new FaceCrop(this);
                    faceCrop.setFaceCropAsync(profilepic, thePic);*/
                    if (thePic != null) {
                        profilePic.setImageBitmap(thePic);
                    }

                    // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));

                    imagePath = resultUri.getPath();


                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }


        } else {
            switch (requestCode) {


                case SELECT_AADHAR:

                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                        Uri uri = data.getData();
                        aadharPath = uri.getPath();
                        id_addressProof.setText("Aadhar Card Selected");
                    }
                    break;


                case SELECT_POLICE_CERTIFICATE:

                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                        Uri uri = data.getData();
                        policeCertifitcatePath = uri.getPath();
                        policeCertificate.setText("Police Verification certificate selected");
                    }
                    break;

                case SELECT_EXTRA_CERTIFICATE:

                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {
                        Uri uri = data.getData();
                        extraCertificatePath = uri.getPath();
                        extraCertificate.setText("Extra Certificate selected");
                    }
                    break;


                case SELECT_PICTURE:
                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                        Uri uri = data.getData();

                        try {
                      /*  thumbnail_r = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // profilepic.setImageBitmap(thumbnail_r);
                        //updateImage(uri);
                        FaceCrop faceCrop = new FaceCrop(this);

                        faceCrop.setFaceCropAsync(profilepic, thumbnail_r);


                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(thumbnail_r, 700, 700,
                                false);

                        // rotated
                        thumbnail_r = imageOreintationValidator(resizedBitmap,
                                uri.getPath());
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, uri.getPath());*/
                     /*   croppedImageUri = uri;
                        Intent cropIntent = new Intent("com.android.camera.action.CROP");
                        //indicate image type and Uri
                        cropIntent.setDataAndType(uri, "image*//*");
                        //set crop properties
                        cropIntent.putExtra("crop", "true");
                        //indicate aspect of desired crop
                        cropIntent.putExtra("aspectX", 1);
                        cropIntent.putExtra("aspectY", 1);
                        //indicate output X and Y
                        cropIntent.putExtra("outputX", 256);
                        cropIntent.putExtra("outputY", 256);
                        //retrieve data on return
                        cropIntent.putExtra("return-data", true);
                        //start the activity - we handle returning in onActivityResult
                        startActivityForResult(cropIntent, PIC_CROP);*/
                            beginCrop(data.getData());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;


                case TAKE_PICTURE:
                    if (resultCode == RESULT_OK) {

                        previewCapturedImage();

                    }

                    break;

                case TAKE_AADHAR:
                    if (resultCode == RESULT_OK) {

                        aadharPath = fileUri.getPath();
                        id_addressProof.setText("Aadhar card Selected");

                    }

                    break;

                case TAKE_POLICE_CERTIFITCATE:

                    if (resultCode == RESULT_OK) {
                        policeCertifitcatePath = fileUri.getPath();
                        policeCertificate.setText("Police certificate selected");
                    }
                    break;

                case TAKE_EXTRA_CERTIFICATE:

                    if (resultCode == RESULT_OK) {
                        extraCertificatePath = fileUri.getPath();
                        extraCertificate.setText("Extra Certificate selected");
                    }
                    break;


            }
        }


    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        // Crop.of(source, destination).asSquare().start(this);

        CropImage.activity(source)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(16, 16)
                .setRequestedSize(1080, 1080)
                .start(this);

    }

    @SuppressLint("NewApi")
    private void previewCapturedImage() {
        try {
            // hide video preview

            //image.setVisibility(View.VISIBLE);

            // bimatp factory
           /* BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500,
                    false);

            // rotated
            thumbnail_r = imageOreintationValidator(resizedBitmap,
                    fileUri.getPath());

            //   updateImage(fileUri);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
            progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
            uploadDocument(byteArray, fileUri.getPath());

            FaceCrop faceCrop = new FaceCrop(this);
            faceCrop.setFaceCropAsync(profilepic, thumbnail_r);

            // profilepic.setImageBitmap(thumbnail_r);
            //image.setBackground(null);
            //image.setImageBitmap(thumbnail_r);
            // IsImageSet = true;*/
          /*  croppedImageUri = fileUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image*//*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);*/
            beginCrop(fileUri);


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

}
