package com.grihachikitsa.gcpatner.Activites.Activites;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.UpdateCareTakerDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {


    private EditText organisationName, conceredPerson, mobile, email, message;
    private Button register;
   private String type="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        if (getSupportActionBar() != null) {
            android.support.v7.app.ActionBar bar = getSupportActionBar();
            bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorPrimary)));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        Intent intent = getIntent();
        if(intent!=null)
        {
            if(intent.hasExtra("user"))
            {
                type = intent.getStringExtra("user");
            }
        }


        elementIntilization();
        onClickListeners();
    }

    public void elementIntilization() {
        //EDITTEXT INTILIZATION
        organisationName = (EditText) findViewById(R.id.organisationName);
        conceredPerson = (EditText) findViewById(R.id.concerendPerson);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        message = (EditText) findViewById(R.id.message);

        //BUTTON INTILIZATION
        register = (Button) findViewById(R.id.register);
    }

    public void onClickListeners() {

        //BUTTON ONCLICK LISTENERS
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!organisationName.getText().toString().isEmpty()) {
                    if (!conceredPerson.getText().toString().isEmpty()) {
                        if (!email.getText().toString().isEmpty()) {

                            if (!mobile.getText().toString().isEmpty()) {
                                if (isValidEmail(email.getText().toString())) {
                                    if (isValidPhoneNumber(mobile.getText().toString())) {
                                        createOrganisation();
                                    } else {
                                        Toast.makeText(RegisterActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();

                                }


                            } else {
                                Toast.makeText(RegisterActivity.this, "Please enter mobile number", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(RegisterActivity.this, "Please enter email", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, "Please enter concerned person name", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, "Please enter organisation name", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean isValidEmail(CharSequence email) {
        if (!TextUtils.isEmpty(email)) {
            return Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
        return false;
    }

    private boolean isValidPhoneNumber(CharSequence phoneNumber) {

        if (phoneNumber.length() < 6 || phoneNumber.length() > 13) {
            return false;
        } else {
            if (!TextUtils.isEmpty(phoneNumber)) {
                return Patterns.PHONE.matcher(phoneNumber).matches();
            }
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // API 5+ solution
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void createOrganisation() {
        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(RegisterActivity.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.createOrganisation(organisationName.getText().toString(), conceredPerson.getText().toString(), email.getText().toString(), mobile.getText().toString(), message.getText().toString(),type);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                if (response != null && response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    customProgressDialog.cancel();

                    if(response.body().getMessage()!=null && !response.body().getMessage().isEmpty())
                    {
                        showMessageDialog(response.body().getMessage(),null,RegisterActivity.this);

                    }
                    else
                    {
                        showMessageDialog("Registered successfully,Our team will contact you soon",null,RegisterActivity.this);
                    }

                } else {
                    customProgressDialog.cancel();
                    if(response!=null && response.body()!=null && response.body().getMessage()!=null && !response.body().getMessage().isEmpty())
                    {
                        Toast.makeText(RegisterActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else
                    {
                        Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(RegisterActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void showMessageDialog(String message, final Intent intent, Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_message);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        Button ok = (Button) dialog.findViewById(R.id.ok);
        TextView messageTextview = (TextView) dialog.findViewById(R.id.message);
        messageTextview.setText(message);


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (intent != null) {
                    startActivity(intent);
                } else {
                    onBackPressed();
                }
            }
        });


        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);
    }


}
