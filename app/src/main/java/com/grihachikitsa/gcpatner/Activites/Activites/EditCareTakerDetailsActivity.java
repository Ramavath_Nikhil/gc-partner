package com.grihachikitsa.gcpatner.Activites.Activites;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetFragment_ChooseImage;
import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetFragment_ChooseImageProfileEdit;
import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetSelectionFragment;
import com.grihachikitsa.gcpatner.Activites.Fragments.BottomSheetSelectionFragmentProfileEdit;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakerCompleteDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.CareTakersResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.OngoingProjectsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.UpdateCareTakerDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Config;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditCareTakerDetailsActivity extends AppCompatActivity {


    private EditText first_name, last_name, email, mobile, gender, age, doj, qualification, experience, designation, address, residenceNumber, id_addressProof, bloodGroup, father, mother, spouse, martialStatus, childre, backgroundVerification;
    private Button addCareTaker;
    private CareTakersResponseModel careTakersResponseModel;
    private Uri fileUri;
    public static int MEDIA_TYPE_IMAGE = 22;
    public static String IMAGE_DIRECTORY_NAME = "grihachikitsa";
    private int PICK_IMAGES = 1;
    final int SELECT_PICTURE = 2, TAKE_PICTURE = 3;
    private Bitmap thumbnail_r;
    private static final int REQUEST_WRITE_STORAGE = 112;
    private String imagePath = "";
    private CircleImageView profilePic;
    private FloatingActionButton profilePicEdit;
    private String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_care_taker_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if (toolbar != null) {
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
            }

        }
        elementsIntilization();
        onClickListener();
        getCareID();
    }

    public void elementsIntilization() {
        //EDITTEXT INTILIZATION
        first_name = (EditText) findViewById(R.id.firstname);
        last_name = (EditText) findViewById(R.id.lastname);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        gender = (EditText) findViewById(R.id.gender);
        age = (EditText) findViewById(R.id.age);
        doj = (EditText) findViewById(R.id.doj);
        qualification = (EditText) findViewById(R.id.qualification);
        experience = (EditText) findViewById(R.id.experience);
        designation = (EditText) findViewById(R.id.designation);
        residenceNumber = (EditText) findViewById(R.id.residncenumber);
        id_addressProof = (EditText) findViewById(R.id.addressProof);
        bloodGroup = (EditText) findViewById(R.id.bloodgroup);
        father = (EditText) findViewById(R.id.father);
        mother = (EditText) findViewById(R.id.mother);
        martialStatus = (EditText) findViewById(R.id.martial_status);
        spouse = (EditText) findViewById(R.id.spouse);
        childre = (EditText) findViewById(R.id.children);
        backgroundVerification = (EditText) findViewById(R.id.backgroundVerification);
        address = (EditText) findViewById(R.id.address);

        //BUTTON ON CLICK LISTENER
        addCareTaker = (Button) findViewById(R.id.addCareTaker);

        //CIRCULAT IMAGE VIEW
        profilePic = (CircleImageView) findViewById(R.id.profilepic);

        //FLOATING BUTTON INTILIZATION
        profilePicEdit = (FloatingActionButton) findViewById(R.id.edit);
    }

    public void onClickListener() {
        addCareTaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkRequiredFields();
            }
        });

        //EDITEXT ONCLICK LISTENERS
        doj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePicker();
            }
        });

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> gender = new ArrayList<String>();
                gender.add("Male");
                gender.add("Female");
                gender.add("Other");
                final BottomSheetSelectionFragmentProfileEdit myBottomSheet = BottomSheetSelectionFragmentProfileEdit.newInstance(gender, "gender");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });

        martialStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> martialStatus = new ArrayList<String>();
                martialStatus.add("Single");
                martialStatus.add("Married");
                martialStatus.add("Divorced");
                martialStatus.add("Widow");
                final BottomSheetSelectionFragmentProfileEdit myBottomSheet = BottomSheetSelectionFragmentProfileEdit.newInstance(martialStatus, "martialStatus");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());
            }
        });

        backgroundVerification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> backgroundVerification = new ArrayList<String>();
                backgroundVerification.add("Verified");
                backgroundVerification.add("Not Verified");

                final BottomSheetSelectionFragmentProfileEdit myBottomSheet = BottomSheetSelectionFragmentProfileEdit.newInstance(backgroundVerification, "backgroundVerification");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });
        bloodGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                List<String> bloodGroup = new ArrayList<String>();
                bloodGroup.add("A +");
                bloodGroup.add("A -");
                bloodGroup.add("B +");
                bloodGroup.add("B -");
                bloodGroup.add("O +");
                bloodGroup.add("O -");
                bloodGroup.add("AB +");
                bloodGroup.add("AB -");

                final BottomSheetSelectionFragmentProfileEdit myBottomSheet = BottomSheetSelectionFragmentProfileEdit.newInstance(bloodGroup, "bloodGroup");
                myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

            }
        });


        //FLOATING ACTTION BUTTON ONCLICK LISTENER
        profilePicEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(EditCareTakerDetailsActivity.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    Log.d("inside permission", "true");
                    ActivityCompat.requestPermissions(EditCareTakerDetailsActivity.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImageProfileEdit myBottomSheet = BottomSheetFragment_ChooseImageProfileEdit.newInstance("Modal Bottom Sheet");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }

            }
        });

        //CIRCULAT IMAGEVIEW ONCLICK LISTENER
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean hasPermission = (ContextCompat.checkSelfPermission(EditCareTakerDetailsActivity.this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

                if (!hasPermission) {
                    ActivityCompat.requestPermissions(EditCareTakerDetailsActivity.this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA},
                            REQUEST_WRITE_STORAGE);
                } else {

                    final BottomSheetFragment_ChooseImageProfileEdit myBottomSheet = BottomSheetFragment_ChooseImageProfileEdit.newInstance("Modal Bottom Sheet");
                    myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                }

            }
        });

    }

    public void checkRequiredFields() {
        if (!first_name.getText().toString().isEmpty()) {
            if (!last_name.getText().toString().isEmpty()) {
                if (!last_name.getText().toString().isEmpty()) {
                    if (!gender.getText().toString().isEmpty()) {
                        if (!age.getText().toString().isEmpty()) {
                            if (!age.getText().toString().isEmpty()) {
                                if (!age.getText().toString().isEmpty()) {
                                    if (!experience.getText().toString().isEmpty()) {
                                        if (!designation.getText().toString().isEmpty()) {
                                            if (!id_addressProof.getText().toString().isEmpty()) {
                                                if (!bloodGroup.getText().toString().isEmpty()) {
                                                    if (!martialStatus.getText().toString().isEmpty()) {
                                                        if (!martialStatus.getText().toString().isEmpty()) {

                                                            if (!address.getText().toString().isEmpty()) {


                                                                if (backgroundVerification.getText().toString().equalsIgnoreCase("verified")) {
                                                                    saveUserDetails("1");
                                                                } else {
                                                                    saveUserDetails("0");
                                                                }


                                                            } else {
                                                                Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide address details", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide martial details", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide martial status details", Toast.LENGTH_SHORT).show();

                                                    }
                                                } else {
                                                    Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide blood group details", Toast.LENGTH_SHORT).show();

                                                }
                                            } else {
                                                Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide ID/Address proof details", Toast.LENGTH_SHORT).show();

                                            }
                                        } else {
                                            Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide designation details", Toast.LENGTH_SHORT).show();

                                        }
                                    } else {
                                        Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide experience details", Toast.LENGTH_SHORT).show();

                                    }
                                } else {
                                    Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide age details", Toast.LENGTH_SHORT).show();

                                }
                            } else {
                                Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide age details", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide age details", Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide gender details", Toast.LENGTH_SHORT).show();

                    }
                } else {
                    Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide last name ", Toast.LENGTH_SHORT).show();

                }
            } else {
                Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide last name", Toast.LENGTH_SHORT).show();

            }
        } else {
            Toast.makeText(EditCareTakerDetailsActivity.this, "Please provide first name", Toast.LENGTH_SHORT).show();

        }


    }


    public void datePicker() {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        doj.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void getCareID() {
        Intent intent = getIntent();
        if (intent != null) {
            String careTakerID = intent.getStringExtra("careTaker");
            getCareTakerDetails(careTakerID);
        }
    }

    public void getCareTakerDetails(String id) {
        final CustomProgressDialog progressBar = CustomProgressDialog.show(EditCareTakerDetailsActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<CareTakerCompleteDetailsModel> call = apiService.getCareTakerCompleteDetails(id);
        call.enqueue(new Callback<CareTakerCompleteDetailsModel>() {
            @Override
            public void onResponse(Call<CareTakerCompleteDetailsModel> call, Response<CareTakerCompleteDetailsModel> response) {

                if (response.body() != null && response.body().getError().equalsIgnoreCase("false")) {

                    if (response.body().getTasks() != null && response.body().getTasks().size() > 0) {
                        progressBar.cancel();
                        printUserDetails(response.body().getTasks().get(0));
                    } else {
                        progressBar.cancel();
                    }

                } else {
                    progressBar.cancel();

                    Log.d("response", "null response");
                }
            }

            @Override
            public void onFailure(Call<CareTakerCompleteDetailsModel> call, Throwable t) {

                t.printStackTrace();
                progressBar.cancel();

            }


        });


    }


    public void printUserDetails(CareTakerCompleteDetailsModel careTakerCompleteDetailsModel) {
        if (careTakerCompleteDetailsModel != null) {


            user_id = careTakerCompleteDetailsModel.getId();
            String[] name = careTakerCompleteDetailsModel.getCaretaker_name().split("\\s+");

            if (name != null && name.length == 2) {
                first_name.setText(checkNull(name[0]));
                last_name.setText(checkNull(name[1]));
            } else if (name != null && name.length == 1) {
                first_name.setText(checkNull(name[0]));
                last_name.setText("");
            } else {
                first_name.setText("");
                last_name.setText("");
            }

            email.setText(checkNull(careTakerCompleteDetailsModel.getEmail()));
            mobile.setText(checkNull(careTakerCompleteDetailsModel.getMobile()));
            gender.setText(checkNull(careTakerCompleteDetailsModel.getGender()));
            age.setText(checkNull(careTakerCompleteDetailsModel.getAge()));
            doj.setText(checkNull(careTakerCompleteDetailsModel.getDOJ()));
            qualification.setText(checkNull(careTakerCompleteDetailsModel.getQualification()));
            experience.setText(checkNull(careTakerCompleteDetailsModel.getExperience()));
            designation.setText(checkNull(careTakerCompleteDetailsModel.getDesignation()));
            residenceNumber.setText(checkNull(careTakerCompleteDetailsModel.getResidence_number()));
            if(careTakerCompleteDetailsModel.getId_addressProof()!=null && !careTakerCompleteDetailsModel.getId_addressProof().isEmpty())
            {
                id_addressProof.setText("Address proof submitted");
            }
            else
            {
                id_addressProof.setText("");
            }

            bloodGroup.setText(checkNull(careTakerCompleteDetailsModel.getBlood_group()));
            father.setText(checkNull(careTakerCompleteDetailsModel.getFather()));
            mother.setText(checkNull(careTakerCompleteDetailsModel.getMother()));
            spouse.setText(checkNull(careTakerCompleteDetailsModel.getSpouse()));
            childre.setText(checkNull(careTakerCompleteDetailsModel.getChildren()));
            address.setText(checkNull(careTakerCompleteDetailsModel.getAddress()));
            martialStatus.setText(checkNull(careTakerCompleteDetailsModel.getMartial_status()));

            if (careTakerCompleteDetailsModel.getBackground_verification() != null && !careTakerCompleteDetailsModel.getBackground_verification().isEmpty()) {
                if (careTakerCompleteDetailsModel.getBackground_verification().equalsIgnoreCase("1"))
                    backgroundVerification.setText("Verified");
                else if (careTakerCompleteDetailsModel.getBackground_verification().equalsIgnoreCase("0"))
                    backgroundVerification.setText("Not Verified");
            }

        } else {
            Toast.makeText(EditCareTakerDetailsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
        }

        Picasso.with(EditCareTakerDetailsActivity.this).load(Config.BASE_URL+careTakerCompleteDetailsModel.getProfile_pic()).placeholder(R.drawable.ic_person).error(R.drawable.ic_person).into(profilePic);
    }



    public  void updateProfilePicture(String imagePath)
    {

        final CustomProgressDialog progressDialog = CustomProgressDialog.show(EditCareTakerDetailsActivity.this);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        //File creating from selected URL
        File file = new File(imagePath);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("uploaded_file", file.getName(), requestFile);
        RequestBody requestID =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),user_id);


        Call<RegisterResponseModel> resultCall = apiService.updateCareTakerProfilePicture(body,requestID);
        resultCall.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {
                progressDialog.cancel();


               /* if(response!=null && response.body()!=null&&response.body().getError().equalsIgnoreCase("false"))
                {
                    Toast.makeText(RegisterActivity.this,"successfull",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Toast.makeText(RegisterActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
                }*/
                Toast.makeText(EditCareTakerDetailsActivity.this,"Message: "+checkNull(response.body().getMessage()),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {

                progressDialog.cancel();
                Toast.makeText(EditCareTakerDetailsActivity.this,"Something went wrong",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty()) {
            return s;
        } else
            return "";
    }


    public void saveUserDetails(String backgroundVerification) {

        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(EditCareTakerDetailsActivity.this);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<UpdateCareTakerDetailsModel> call = apiService.updateCareTakerDetails(first_name.getText().toString() + " " + last_name.getText().toString(), user_id, email.getText().toString(), mobile.getText().toString(), gender.getText().toString(), age.getText().toString(), doj.getText().toString()
                , qualification.getText().toString(), experience.getText().toString(), designation.getText().toString(), id_addressProof.getText().toString(), bloodGroup.getText().toString(), martialStatus.getText().toString(), backgroundVerification
                , address.getText().toString(), residenceNumber.getText().toString(), father.getText().toString(), mother.getText().toString(), spouse.getText().toString(), childre.getText().toString());
        call.enqueue(new Callback<UpdateCareTakerDetailsModel>() {
            @Override
            public void onResponse(Call<UpdateCareTakerDetailsModel> call, Response<UpdateCareTakerDetailsModel> response) {

                if (response != null && response.body() != null && response.body().getError().equalsIgnoreCase("false")) {
                    customProgressDialog.cancel();
                    Toast.makeText(EditCareTakerDetailsActivity.this, "updated succesfully", Toast.LENGTH_SHORT).show();
                } else {
                    customProgressDialog.cancel();
                    if (response != null && response.body() != null && response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(EditCareTakerDetailsActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditCareTakerDetailsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<UpdateCareTakerDetailsModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(EditCareTakerDetailsActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void setText(String type, String selectedText) {
        if (type.equalsIgnoreCase("gender")) {
            gender.setText(selectedText);

        } else if (type.equalsIgnoreCase("backgroundVerification")) {
            backgroundVerification.setText(selectedText);

        } else if (type.equalsIgnoreCase("bloodGroup")) {
            bloodGroup.setText(selectedText);

        } else if (type.equalsIgnoreCase("martialStatus")) {
            if (selectedText.equalsIgnoreCase("single")) {
                spouse.setVisibility(View.GONE);
                childre.setVisibility(View.GONE);
            } else {
                spouse.setVisibility(View.VISIBLE);
                childre.setVisibility(View.VISIBLE);
            }

            martialStatus.setText(selectedText);
        }
    }



    public void takePicture() {

        Intent intent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                /*
                 * File photo = new
                 * File(Environment.getExternalStorageDirectory(),
                 * "Pic.jpg"); intent.putExtra(MediaStore.EXTRA_OUTPUT,
                 * Uri.fromFile(photo)); imageUri = Uri.fromFile(photo);
                 */
        // startActivityForResult(intent,TAKE_PICTURE);

        Intent intents = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intents.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intents, TAKE_PICTURE);
    }


    public void selectgImageFromGallery() {
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select Picture"),
                SELECT_PICTURE);

    }


    // for roted image......
    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        } catch (OutOfMemoryError err) {
            source.recycle();
            Date d = new Date();
            CharSequence s = DateFormat
                    .format("MM-dd-yy-hh-mm-ss", d.getTime());
            String fullPath = Environment.getExternalStorageDirectory()
                    + "/RYB_pic/" + s.toString() + ".jpg";
            if ((fullPath != null) && (new File(fullPath).exists())) {
                new File(fullPath).delete();
            }
            bitmap = null;
            err.printStackTrace();
        }
        return bitmap;
    }


    public static Bitmap decodeSampledBitmapFromResource(String pathToFile,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pathToFile, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        Log.e("inSampleSize", "inSampleSize______________in storage"
                + options.inSampleSize);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(pathToFile, options);
    }


    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

        }

        return inSampleSize;
    }


    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }

        return mediaFile;
    }


    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    public void startImageSelection() {
        Intent intent = new Intent();

        intent.setType("image/*");


        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap thePic = null;
                try {
                    thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);


                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                    byte[] byteArray = stream.toByteArray();

                  /*  FaceCrop faceCrop = new FaceCrop(this);
                    faceCrop.setFaceCropAsync(profilepic, thePic);*/
                    if (thePic != null) {
                        profilePic.setImageBitmap(thePic);
                    }

                    // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));

                    imagePath = resultUri.getPath();
                    updateProfilePicture(resultUri.getPath());

                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }


        } else {
            switch (requestCode) {



         /*   case Crop.REQUEST_CROP:


                    try {
                        Bundle extras = data.getExtras();

                        Bitmap thePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Crop.getOutput(data));

                        FaceCrop faceCrop = new FaceCrop(this);
                        faceCrop.setFaceCropAsync(profilepic, thePic);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thePic.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        byte[] byteArray = stream.toByteArray();

                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, Crop.getOutput(data).getPath());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                break;
*/

                case SELECT_PICTURE:
                    if (resultCode == RESULT_OK && data != null && data.getData() != null) {

                        Uri uri = data.getData();

                        try {
                      /*  thumbnail_r = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        // profilepic.setImageBitmap(thumbnail_r);
                        //updateImage(uri);
                        FaceCrop faceCrop = new FaceCrop(this);

                        faceCrop.setFaceCropAsync(profilepic, thumbnail_r);


                        Bitmap resizedBitmap = Bitmap.createScaledBitmap(thumbnail_r, 700, 700,
                                false);

                        // rotated
                        thumbnail_r = imageOreintationValidator(resizedBitmap,
                                uri.getPath());
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
                        // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
                        uploadDocument(byteArray, uri.getPath());*/
                     /*   croppedImageUri = uri;
                        Intent cropIntent = new Intent("com.android.camera.action.CROP");
                        //indicate image type and Uri
                        cropIntent.setDataAndType(uri, "image*//*");
                        //set crop properties
                        cropIntent.putExtra("crop", "true");
                        //indicate aspect of desired crop
                        cropIntent.putExtra("aspectX", 1);
                        cropIntent.putExtra("aspectY", 1);
                        //indicate output X and Y
                        cropIntent.putExtra("outputX", 256);
                        cropIntent.putExtra("outputY", 256);
                        //retrieve data on return
                        cropIntent.putExtra("return-data", true);
                        //start the activity - we handle returning in onActivityResult
                        startActivityForResult(cropIntent, PIC_CROP);*/
                            beginCrop(data.getData());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;


                case TAKE_PICTURE:
                    if (resultCode == RESULT_OK) {

                        previewCapturedImage();

                    }

                    break;


            }
        }


    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        // Crop.of(source, destination).asSquare().start(this);

        CropImage.activity(source)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(16, 16)
                .setRequestedSize(1080, 1080)
                .start(this);

    }

    @SuppressLint("NewApi")
    private void previewCapturedImage() {
        try {
            // hide video preview

            //image.setVisibility(View.VISIBLE);

            // bimatp factory
           /* BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 8;

            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);

            Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, 500, 500,
                    false);

            // rotated
            thumbnail_r = imageOreintationValidator(resizedBitmap,
                    fileUri.getPath());

            //   updateImage(fileUri);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail_r.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            // chooseImage.setImageDrawable(getResources().getDrawable(R.drawable.progressdrawable));
            progressDialog = CustomProgressDialog.show(UserProfileEdit.this);
            uploadDocument(byteArray, fileUri.getPath());

            FaceCrop faceCrop = new FaceCrop(this);
            faceCrop.setFaceCropAsync(profilepic, thumbnail_r);

            // profilepic.setImageBitmap(thumbnail_r);
            //image.setBackground(null);
            //image.setImageBitmap(thumbnail_r);
            // IsImageSet = true;*/
          /*  croppedImageUri = fileUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(fileUri, "image*//*");
            //set crop properties
            cropIntent.putExtra("crop", "true");
            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);*/
            beginCrop(fileUri);


        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


}
