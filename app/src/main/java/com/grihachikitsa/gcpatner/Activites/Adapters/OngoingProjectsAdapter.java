package com.grihachikitsa.gcpatner.Activites.Adapters;

/**
 * Created by Nikil on 12/8/2016.
 */

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.grihachikitsa.gcpatner.Activites.Activites.MainActivity;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiClient;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ApiInterface;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.OngoingProjectsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.ProjectCompletDetailsModel;
import com.grihachikitsa.gcpatner.Activites.Retrofit.ResponeModels.RegisterResponseModel;
import com.grihachikitsa.gcpatner.Activites.Utils.Prefs;
import com.grihachikitsa.gcpatner.Activites.Views.CustomProgressDialog;
import com.grihachikitsa.gcpatner.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OngoingProjectsAdapter extends RecyclerView.Adapter<OngoingProjectsAdapter.MyViewHolder> {

    private List<OngoingProjectsModel> ongoingProjectsModelList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {


        public TextView name, age, weight, gender, service, timePeriod, address, startDate, duration,preferedLanguages;
        public Button completeProject;
        public LinearLayout mainlayout;
        public MyViewHolder(View view) {
            super(view);
            //careType = (TextView) view.findViewById(R.id.careType);
            name = (TextView) view.findViewById(R.id.name);
            age = (TextView) view.findViewById(R.id.age);
            weight = (TextView) view.findViewById(R.id.weight);
            service = (TextView) view.findViewById(R.id.serivces);
            timePeriod = (TextView) view.findViewById(R.id.time);
            address = (TextView) view.findViewById(R.id.address);
            startDate = (TextView) view.findViewById(R.id.startdate);
            duration = (TextView) view.findViewById(R.id.end_day);
            gender = (TextView) view.findViewById(R.id.gender);
            completeProject = (Button) view.findViewById(R.id.completeProject);
            preferedLanguages = (TextView) view.findViewById(R.id.preferedLanguage);
            mainlayout = (LinearLayout) view.findViewById(R.id.mainlayout);
        }
    }


    public OngoingProjectsAdapter(List<OngoingProjectsModel> moviesList, Context context) {
        this.ongoingProjectsModelList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_ongoingprojects, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final OngoingProjectsModel ongoingProjectsModel = ongoingProjectsModelList.get(position);
        //  holder.title.setText("You took a ride on "+movie.getCreated_at());


        holder.name.setText(checkNull(ongoingProjectsModel.getFirst_name()) + " " + checkNull(ongoingProjectsModel.getLast_name()));
        holder.address.setText("Address: " + checkNullNA(ongoingProjectsModel.getAddress()));
        holder.timePeriod.setText("Time Period: " + checkNullNA(ongoingProjectsModel.getHours()));
        holder.service.setText("Services: " + checkNullNA(ongoingProjectsModel.getServices()));
        holder.age.setText("Age: " + checkNullNA(ongoingProjectsModel.getAge()));
        holder.weight.setText("Weight: " + checkNullNA(ongoingProjectsModel.getWeight()));
        holder.gender.setText("Gender: " + checkNullNA(ongoingProjectsModel.getGender()));
        holder.startDate.setText("Start Date: " + checkNullNA(ongoingProjectsModel.getCoated_on()));
        holder.duration.setText("Duration: " + checkNullNA(ongoingProjectsModel.getDuration()));
        holder.preferedLanguages.setText("Prefered Languages: "+checkNullNA(ongoingProjectsModel.getFirst_language()));
        if(ongoingProjectsModel.getSecond_language()!=null && !ongoingProjectsModel.getSecond_language().isEmpty())
        {
            holder.preferedLanguages.setText("Prefered Languages: "+checkNullNA(ongoingProjectsModel.getFirst_language() +","+ongoingProjectsModel.getSecond_language()));
        }
        if(ongoingProjectsModel.getThird_language()!=null && !ongoingProjectsModel.getThird_language().isEmpty())
        {
            holder.preferedLanguages.setText("Prefered Languages: "+checkNullNA(ongoingProjectsModel.getFirst_language() +","+ongoingProjectsModel.getSecond_language()+","+ongoingProjectsModel.getThird_language()));
        }

        holder.completeProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            completeProject(ongoingProjectsModel.getProject_id(),position);
            }
        });
        holder.mainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getProjectDetails(ongoingProjectsModel.getProject_id());
            }
        });

    }

    public void getProjectDetails(String project_id) {


        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);

        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ProjectCompletDetailsModel> call = apiService.getCompleteDetailsOfProject(project_id);
        call.enqueue(new Callback<ProjectCompletDetailsModel>() {
            @Override
            public void onResponse(Call<ProjectCompletDetailsModel> call, Response<ProjectCompletDetailsModel> response) {
                customProgressDialog.cancel();
                if (response.body() != null && response.body().getError().equalsIgnoreCase("false") && response.body().getTasks() != null && response.body().getTasks().size() > 0 && response.body().getTasks().get(0) != null) {
                    ProjectCompletDetailsModel projectCompletDetailsModel = response.body().getTasks().get(0);

                    printProjectDetails(projectCompletDetailsModel);

                } else {

                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProjectCompletDetailsModel> call, Throwable t) {

                customProgressDialog.cancel();

                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void printProjectDetails(ProjectCompletDetailsModel projectCompletDetailsModel) {


        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_popup_complete_details_of_project);


        // set the custom dialog components - text, image and button


        TextView patientName, patientAge, patientHeight, patientWeight, patientGender, patientServices, hours, duration, moreinfo, address;
        TextView quotes, careTakerName, organisationName, coatedPrice, acceptedOn;
        Button viewQuotes, careTakerDetails, organisationDetails;
        LinearLayout careTakerDetailsLayout;
        TextView careTakerDetailsTitle;


        //TEXTVIEW INTILIZATION
        patientName = (TextView) dialog.findViewById(R.id.name);
        patientAge = (TextView) dialog.findViewById(R.id.age);
        patientHeight = (TextView) dialog.findViewById(R.id.height);
        patientWeight = (TextView) dialog.findViewById(R.id.weight);
        patientGender = (TextView) dialog.findViewById(R.id.gender);
        patientServices = (TextView) dialog.findViewById(R.id.serivces);
        hours = (TextView) dialog.findViewById(R.id.hours);
        duration = (TextView) dialog.findViewById(R.id.duration);
        moreinfo = (TextView) dialog.findViewById(R.id.moreinfo);
        address = (TextView) dialog.findViewById(R.id.address);
        quotes = (TextView) dialog.findViewById(R.id.quotes);
        careTakerName = (TextView) dialog.findViewById(R.id.careTakerName);
        organisationName = (TextView) dialog.findViewById(R.id.organisationName);
        coatedPrice = (TextView) dialog.findViewById(R.id.coatedPrice);
        acceptedOn = (TextView) dialog.findViewById(R.id.coatedOn);
        careTakerDetailsTitle = (TextView) dialog.findViewById(R.id.careTakerDetails);

        //BUTTON INTILIZATION
        viewQuotes = (Button) dialog.findViewById(R.id.viewQuotes);
        careTakerDetails = (Button) dialog.findViewById(R.id.careTakerCompleteDetails);
        organisationDetails = (Button) dialog.findViewById(R.id.organisationCompleteDetails);

        //LINEAR LAYOUT INTILIZATION
        careTakerDetailsLayout = (LinearLayout) dialog.findViewById(R.id.careTakerDetailsLayout);


        patientName.setText("Patient Name: " + checkNull(projectCompletDetailsModel.getPatient_name()));
        patientAge.setText("Age: " + checkNull(projectCompletDetailsModel.getAge()));
        patientGender.setText("Gender: " + checkNull(projectCompletDetailsModel.getGender()));
        patientHeight.setText("Height: " + checkNull(projectCompletDetailsModel.getHeight()));
        patientWeight.setText("Weight: " + checkNull(projectCompletDetailsModel.getWeight()));
        patientServices.setText("Services: " + checkNull(projectCompletDetailsModel.getServices()));
        hours.setText("Time Period: " + checkNull(projectCompletDetailsModel.getHours()));
        duration.setText("Duration: " + checkNull(projectCompletDetailsModel.getDuration()));
        if (projectCompletDetailsModel.getMoreinfo() != null && !projectCompletDetailsModel.getMoreinfo().isEmpty()) {
            moreinfo.setVisibility(View.VISIBLE);
            moreinfo.setText("More info: " + checkNull(projectCompletDetailsModel.getMoreinfo()));
        } else {
            moreinfo.setVisibility(View.GONE);
        }

        if (projectCompletDetailsModel.getAddress() != null && !projectCompletDetailsModel.getAddress().isEmpty()) {
            address.setVisibility(View.VISIBLE);
            address.setText("Address: " + checkNull(projectCompletDetailsModel.getAddress()));
        } else {
            address.setVisibility(View.GONE);
        }

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(dialog.getWindow().getAttributes());
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ( (MainActivity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;

        int width = displaymetrics.widthPixels;


        dialog.getWindow().setLayout(width, lp.height);


    }
    public void completeProject(String project_id, final int position) {


        final CustomProgressDialog customProgressDialog = CustomProgressDialog.show(context);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<RegisterResponseModel> call = apiService.updateProjectCompletion(project_id);
        call.enqueue(new Callback<RegisterResponseModel>() {
            @Override
            public void onResponse(Call<RegisterResponseModel> call, Response<RegisterResponseModel> response) {

                customProgressDialog.cancel();
                if (response.body() != null && response.body().getError() != null && response.body().getError().equalsIgnoreCase("false")) {
                    if (response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        ongoingProjectsModelList.remove(position);
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(context, "Project completed", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (response.body().getMessage() != null && !response.body().getMessage().isEmpty()) {
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseModel> call, Throwable t) {
                customProgressDialog.cancel();
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String checkNull(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "";
    }

    public String checkNullNA(String s) {
        if (s != null && !s.isEmpty())
            return s;
        else
            return "NA";
    }

    @Override
    public int getItemCount() {
        return ongoingProjectsModelList.size();
    }

    public void updateRefernceStatus() {

    }

}
